/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./video-codec.hpp"

#include "../infotree/json-serializer.hpp"
#include "../quiddity/property/gprop-to-prop.hpp"
#include "../quiddity/quiddity.hpp"
#include "../quiddity/startable.hpp"
#include "../utils/scope-exit.hpp"
#include "./utils.hpp"

namespace switcher {
namespace gst {
VideoCodec::VideoCodec(quiddity::Quiddity* quid,
                       const std::string& shmpath,
                       const std::string& shmpath_encoded)
    : quid_(quid),
      reset_id_(quid_->mmanage<MPtr(&quiddity::method::MBag::make_method<std::function<bool()>>)>(
          "reset",
          infotree::json::deserialize(
              R"(
                  {
                   "name" : "Reset codec configuration",
                   "description" : "Reset codec configuration to default",
                   "arguments" : []
                  }
              )"),
          [this]() { return reset_codec_configuration(); })),
      shmpath_to_encode_(shmpath),
      shm_encoded_path_(shmpath_encoded),
      custom_shmsink_path_(!shmpath_encoded.empty()),
      gst_pipeline_(std::make_unique<Pipeliner>(nullptr, nullptr)),
      codecs_(gst::utils::element_factory_list_to_pair_of_vectors(
                  GST_ELEMENT_FACTORY_TYPE_VIDEO_ENCODER,
                  GST_RANK_NONE,
                  true,
                  {"schroenc", "theoraenc"}),
              0),
      codec_id_(install_codec()),
      param_group_id_(quid_->pmanage<MPtr(&quiddity::property::PBag::make_group)>(
          "codec_params", "Codec configuration", "Codec specific parameters")) {
  set_shm(shmpath);
  // Select default encoder
  set_default_codec();
}

void VideoCodec::hide() {
  quid_->mmanage<MPtr(&quiddity::method::MBag::disable)>(
      reset_id_, quiddity::Startable::disabledWhenStartedMsg);
  quid_->pmanage<MPtr(&quiddity::property::PBag::disable)>(
      codec_id_, quiddity::Startable::disabledWhenStartedMsg);
  quid_->pmanage<MPtr(&quiddity::property::PBag::disable)>(
      param_group_id_, quiddity::Startable::disabledWhenStartedMsg);
}

void VideoCodec::show() {
  quid_->mmanage<MPtr(&quiddity::method::MBag::enable)>(reset_id_);
  quid_->pmanage<MPtr(&quiddity::property::PBag::enable)>(codec_id_);
  quid_->pmanage<MPtr(&quiddity::property::PBag::enable)>(param_group_id_);
}

void VideoCodec::make_bin() {
  // When dealing with h.264 codecs, ask for byte-stream stream-format and select main profile
  if (codec_name_.find("264") != std::string::npos) {
    GstCaps* h264_streamformat_and_profile_caps = gst_caps_from_string(
        "video/x-h264, stream-format=(string)byte-stream, profile=(string)constrained-baseline");
    g_object_set(G_OBJECT(output_capsfilter_.get_raw()),
                 "caps",
                 h264_streamformat_and_profile_caps,
                 nullptr);
    gst_caps_unref(h264_streamformat_and_profile_caps);
  }

  if (0 != codecs_.get_current_index()) {
    gst_bin_add_many(GST_BIN(gst_pipeline_->get_pipeline()),
                     shmsrc_.get_raw(),
                     color_space_codec_element_.get_raw(),
                     codec_element_.get_raw(),
                     output_capsfilter_.get_raw(),
                     shm_encoded_.get_raw(),
                     nullptr);
    gst_element_link_many(shmsrc_.get_raw(),
                          color_space_codec_element_.get_raw(),
                          codec_element_.get_raw(),
                          output_capsfilter_.get_raw(),
                          shm_encoded_.get_raw(),
                          nullptr);
  }
}

bool VideoCodec::remake_codec_elements() {
  if (0 != codecs_.get_current_index()) {
    if (!UGstElem::renew(shmsrc_, {"socket-path"}) ||
        !UGstElem::renew(shm_encoded_, {"socket-path", "sync", "async", "extra-caps-properties"}) ||
        !UGstElem::renew(color_space_codec_element_) ||
        !UGstElem::renew(codec_element_, codec_properties_)) {
      quid_->sw_error("Failed to remake the codec element.");
      return false;
    }
    // all these check before may have passed but we may have a nullptr for the codec element
    if (codec_element_.get_raw() == nullptr) {
      quid_->sw_error(
          "The gst codec element is nullptr. Your GPU may be unavailable. Try a software "
          "encoder. ");
      return false;
    }
  }  // end no codec
  return true;
}

void VideoCodec::remove_codec_properties() {
  for (auto& it : codec_properties_)
    quid_->pmanage<MPtr(&quiddity::property::PBag::remove)>(
        quid_->pmanage<MPtr(&quiddity::property::PBag::get_id)>(it));
  codec_properties_.clear();
}

void VideoCodec::make_codec_properties() {
  remove_codec_properties();
  guint num_properties = 0;
  GParamSpec** props =
      g_object_class_list_properties(G_OBJECT_GET_CLASS(codec_element_.get_raw()), &num_properties);
  On_scope_exit { g_free(props); };
  for (guint i = 0; i < num_properties; i++) {
    auto param_name = g_param_spec_get_name(props[i]);
    if (param_black_list_.end() == param_black_list_.find(param_name)) {
      quid_->pmanage<MPtr(&quiddity::property::PBag::push_parented)>(
          param_name,
          "codec_params",
          quiddity::property::to_prop(G_OBJECT(codec_element_.get_raw()), param_name));
      codec_properties_.push_back(param_name);
    }
  }
}

bool VideoCodec::reset_codec_configuration() {
  set_default_codec();
  // notify new codec selection
  quid_->pmanage<MPtr(&quiddity::property::PBag::notify)>(codec_id_);
  return true;
}

bool VideoCodec::start(switcher::quiddity::claw::sfid_t sfid) {
  hide();
  if (0 == quid_
               ->pmanage<MPtr(&quiddity::property::PBag::get<quiddity::property::IndexOrName>)>(
                   codec_id_)
               .index_)
    return true;
  shmsink_sub_ = std::make_unique<shmdata::GstTreeUpdater>(
      quid_, shm_encoded_.get_raw(), shm_encoded_path_, shmdata::Direction::writer);
  shmsrc_sub_ =
      std::make_unique<shmdata::GstTreeUpdater>(quid_, shmsrc_.get_raw(), shmpath_to_encode_, sfid);
  make_bin();

  g_object_set(G_OBJECT(gst_pipeline_->get_pipeline()), "async-handling", TRUE, nullptr);
  if (copy_buffers_) g_object_set(G_OBJECT(shmsrc_.get_raw()), "copy-buffers", TRUE, nullptr);
  gst_pipeline_->play(true);
  return true;
}

bool VideoCodec::stop() {
  show();
  if (0 != quid_
               ->pmanage<MPtr(&quiddity::property::PBag::get<quiddity::property::IndexOrName>)>(
                   codec_id_)
               .index_) {
    shmsink_sub_.reset();
    shmsrc_sub_.reset();
    if (!remake_codec_elements()) {
      return false;
    }
    make_codec_properties();
    gst_pipeline_ = std::make_unique<Pipeliner>(nullptr, nullptr);
  }
  return true;
}

void VideoCodec::set_shm(const std::string& shmpath) {
  shmpath_to_encode_ = shmpath;
  if (!custom_shmsink_path_) shm_encoded_path_ = shmpath_to_encode_ + "-encoded";
  g_object_set(G_OBJECT(shmsrc_.get_raw()),
               "do-timestamp",
               TRUE,
               "socket-path",
               shmpath_to_encode_.c_str(),
               nullptr);

  auto extra_caps = quid_->get_quiddity_caps();
  // export encoder name in shmsink caps
  extra_caps = extra_caps + ",codec=(string)" + codec_name_;
  g_object_set(G_OBJECT(shm_encoded_.get_raw()),
               "socket-path",
               shm_encoded_path_.c_str(),
               "sync",
               FALSE,
               "async",
               FALSE,
               "extra-caps-properties",
               extra_caps.c_str(),
               nullptr);
}

void VideoCodec::apply_x264_default_settings() {
  // x264 needs shmdata to copy buffers
  copy_buffers_ = true;

  // Use byte-stream stream-format instead of avc
  g_object_set(G_OBJECT(codec_element_.get_raw()), "byte-stream", TRUE, nullptr);
  // Default to 2 Mbps
  g_object_set(G_OBJECT(codec_element_.get_raw()), "bitrate", 2048, nullptr);
  // Use zerolatency preset non-psychovisual tuning options
  gst_util_set_object_arg(G_OBJECT(codec_element_.get_raw()), "tune", "zerolatency");
  // Use ultrafast for speed/quality tradeoff options
  gst_util_set_object_arg(G_OBJECT(codec_element_.get_raw()), "speed-preset", "ultrafast");
  // Use Constant Bitrate encoding pass/type
  gst_util_set_object_arg(G_OBJECT(codec_element_.get_raw()), "pass", "cbr");
  // Do not automatically decide how many B-frames to use
  g_object_set(G_OBJECT(codec_element_.get_raw()), "b-adapt", FALSE, nullptr);
  // Use short vbv buffer targeting 120ms instead of 600ms default
  g_object_set(G_OBJECT(codec_element_.get_raw()), "vbv-buf-capacity", 120, nullptr);
}

void VideoCodec::apply_nvenc_default_settings() {
  /*   Recommended NVENC settings from NVIDIA documentation for low-latency use cases like
     game-streaming, video conferencing etc.
        https://docs.nvidia.com/video-technologies/video-codec-sdk/12.0/nvenc-video-encoder-api-prog-guide/index.html#recommended-nvenc-settings

        - Ultra-low latency or low latency Tuning Info
          - NV_ENC_TUNING_INFO_ULTRA_LOW_LATENCY option not exposed by GStreamer. NVENC header
     comment mention "TuningInfo is not applicable to H264 and HEVC MEOnly mode".
        - Rate control mode = CBR
          - Using cbr-ld-hq (Low-Delay CBR, High Quality)
        - Multi Pass – Quarter/Full (evaluate and decide)
          - NV_ENC_TWO_PASS_QUARTER_RESOLUTION enabled when using cbr-ld-hq (Low-Delay CBR, High
     Quality) rc-mode
        - Very low VBV buffer size (e.g. single frame = bitrate/framerate)
        - Use short vbv buffer targeting 120ms at 2 Mbps. vbv-buffer-size of 250 kilobits gives
    around 120ms at 2048 Kbit/s.
        - No B Frames
        - Infinite GOP length
          - Tests shows that x264 software decoder cannot decode infinite GOP length, using default
     of 30
        - Adaptive quantization (AQ) enabled**
          - Using spatial-aq (Spatial Adaptive Quantization) and temporal-aq (Temporal Adaptive
     Quantization)
        - Long term reference pictures***
          - GStreamer does not expose enableLTR flag yet
        - Intra refresh***
          - GStreamer does not expose enableIntraRefresh flag yet
        - Non-reference P frames***
          - GStreamer does not expose hierarchicalPFrames flag yet
        - Force IDR***
          - Already implemented, if encoder is asked GST_VIDEO_CODEC_FRAME_IS_FORCE_KEYFRAME, IDR
     will be generated
        ***: These features are useful for error recovery during transmission across noisy mediums.
   */

  // Disable using AU (Access Unit) delimiter
  g_object_set(G_OBJECT(codec_element_.get_raw()), "aud", FALSE, nullptr);
  // Use 0 B-frames between I and P
  g_object_set(G_OBJECT(codec_element_.get_raw()), "b-frames", 0, nullptr);
  // Default to 2 Mbps
  g_object_set(G_OBJECT(codec_element_.get_raw()), "bitrate", 2048, nullptr);
  // Use 30 frames between intra frames
  g_object_set(G_OBJECT(codec_element_.get_raw()), "gop-size", 30, nullptr);
  // Use low-latency-hp (5) (Low Latency, High Performance) preset
  gst_util_set_object_arg(G_OBJECT(codec_element_.get_raw()), "preset", "low-latency-hp");
  // Use cbr-ld-hq (3) (Low-Delay CBR, High Quality) rate control
  gst_util_set_object_arg(G_OBJECT(codec_element_.get_raw()), "rate-control", "cbr-ld-hq");
  // Insert sequence headers (SPS/PPS) per IDR, needed when encoder is started before decoder
  // receive stream.
  g_object_set(G_OBJECT(codec_element_.get_raw()), "repeat-sequence-header", TRUE, nullptr);
  // Use Spatial Adaptive Quantization
  g_object_set(G_OBJECT(codec_element_.get_raw()), "spatial-aq", TRUE, nullptr);
  // Use Temporal Adaptive Quantization
  g_object_set(G_OBJECT(codec_element_.get_raw()), "temporal-aq", TRUE, nullptr);
  // Use short vbv buffer targeting 120ms at 2 Mbps
  // ~250 Kbits at 2048 Kbit/s give around 120 milliseconds
  g_object_set(G_OBJECT(codec_element_.get_raw()), "vbv-buffer-size", 250, nullptr);
}

bool VideoCodec::set_codec(const quiddity::property::IndexOrName codec_selection) {
  // remove current properties
  remove_codec_properties();

  codecs_.select(codec_selection);
  codec_name_ = codecs_.get_attached();
  codec_element_.mute(codec_name_.c_str());

  if (!remake_codec_elements()) {
    return false;
  }

  // set default x264 settings for video conferencing use
  if (codec_name_ == "x264enc") {
    apply_x264_default_settings();
  }

  // set default nvenc settings for video conferencing use
  if (codec_name_ == "nvcudah264enc") {
    apply_nvenc_default_settings();
  }

  // notify properties after we set default properties
  make_codec_properties();

  return true;
}

const switcher::quiddity::property::IndexOrName VideoCodec::get_codec() { return codecs_.get(); }

quiddity::property::prop_id_t VideoCodec::install_codec() {
  return quid_->pmanage<MPtr(&quiddity::property::PBag::make_selection<>)>(
      "codec",
      [this](const quiddity::property::IndexOrName& codec_selection) {
        // Scenic does not invoke reset method to reset quiddity's properties, it sets them to falsy
        // values. Treat selecting first codec entry 0 ("None" or "") as resetting configuration
        if (codec_selection.index_ == 0) {
          return reset_codec_configuration();
        }

        // codec selection setter
        return set_codec(codec_selection);
      },
      [this]() {
        // codec selection setter
        return get_codec();
      },
      "Video Codecs",
      "Selected video codec for encoding",
      codecs_);
}

bool VideoCodec::set_default_codec() {
  std::vector<std::string> codecs_list = codecs_.get_list();
  // if hardware codec is available, prefer it over software
  if (std::find(codecs_list.begin(), codecs_list.end(), DEFAULT_HARDWARE_CODEC) !=
      codecs_list.end()) {
    quiddity::property::IndexOrName codec_selection(DEFAULT_HARDWARE_CODEC);
    return set_codec(codec_selection);
  }
  quiddity::property::IndexOrName codec_selection(DEFAULT_SOFTWARE_CODEC);
  return set_codec(codec_selection);
}

}  // namespace gst
}  // namespace switcher
