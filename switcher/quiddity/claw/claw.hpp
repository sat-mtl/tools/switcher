/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_QUIDDITY_CLAW_CLAW_H__
#define __SWITCHER_QUIDDITY_CLAW_CLAW_H__

#include <cstdint>  // uint32_t
#include <functional>
#include <shmdata/type.hpp>
#include <vector>

#include "../../utils/safe-bool-idiom.hpp"
#include "../quid-id-t.hpp"
#include "./connection-spec.hpp"
#include "./types.hpp"

namespace switcher {

namespace quiddities {
class ExternalWriter;
}
namespace quiddity {
class Quiddity;
namespace bundle {
class Bundle;
}  // namespace bundle

namespace claw {
/**
 * Claw class provides Quiddity the ability to specify available
 * shmdata, both as writer and as follower. It is designed in
 * order to provide an API to Quiddity user, but also an API
 * for Quiddity authors. It enables connection of Shmdata among
 * Quiddities, but also with raw Shmdata.
 *
 * Claw offers an API for Quiddity user for connection to a
 * shmdata writer. It maintains two sets of identifiers (writer and follower)
 * for each unique shmdata points for the Quiddity.
 *
 * Claw provides introspection mechanism that provides shmdata
 * labels and types, along with methods allowing for evaluation
 * of shmdata connection compatibility.
 *
 * Quiddity author, in order to enable a shmdata into switcher,
 * must provide a ConnectionSpec. The accompanying optional OnConnect_t
 * and OnDisconnect_t callbacks must be implemented for a Quiddity
 * that expects to read from shmdata.
 *
 * Both follower and writer can be "meta". Meta Shmdata have label containing a '%' character.
 * When meta, a Shmdata follower
 * or writer is not an actual Shmdata, but rather the description of a Shmdata generator.
 * For instance a meta writer "video%" may instantiate several video Shmdata, such as
 * "videoLeft", "videoRight", among others. While the number of generated Shmdata from
 * this meta writer is not know in advance, it is expected the actual data streams of generated
 * Shmdata comply with meta Shmdata "can_do" description.
 *
 * While a meta writer initiates new writers from inside the Quiddity implementation,
 * meta follower initiates new Shmdata follower when connect is invoked on the meta follower.
 * For instance, a recorder Quiddity could connect to an undefined number of audio Shmdata, and
 * then perform an aligned recording of these streams.
 */
class Claw : public SafeBoolIdiom {
  friend class bundle::Bundle;  // custom set of connection spec and callbacks
  // we need this in order to be able to use the forced_writer_shmpaths_ to
  // expose the correct shmpath.
  friend class switcher::quiddities::ExternalWriter;

 public:
  /**
   * Claw constructor. A Claw object is designed to be owned and constructed by
   * the Quiddity. Its public const methods are exposed to the Quiddity user, through
   * Make_consultable. Other public methods are dedicated to Quiddity and other subclasses
   *
   * \param quid    Pointer to the Quiddity that owns the
   * Claw.
   * \param spec    The initial ConnectionSpec for the Quiddity
   * \param on_connect_cb the function to call when a Quiddity
   * is asked to connect to a shmdata writer
   * \param on_disconnect_cb the function to call when a Quiddity
   * is asked to disconnect to a shmdata writer
   */
  Claw(Quiddity* quid,
       ConnectionSpec&& spec,
       OnConnect_t on_connect_cb,
       OnDisconnect_t on_disconnect_cb);

  /**
   * \brief Connect a local Shmdata follower with a Shmdata writer of an other Quiddity.
   * also populates the follower_swids_ and follower_writer_ids_ maps with the right values
   *
   * This is the function that will notify that a connection was made. Since connect_quid and
   * try_connect call this function, both these will also generate connection notifications.
   *
   * This means that connect_raw *does not* generate connection notifications.
   *
   * \param local_sfid the Shmdata follower identifier to connect. If it is meta, a new follower
   * will be allocated from it. local_sfid must be allocated by the current Claw
   * \param writer_quid the identifier of the Quiddity hosting the Shmdata writer
   * \param writer_swid the identifier of the Shmdata writer, allocated by the Claw from the
   * Quiddity identified by writer_quid
   *
   * \return the identifier of the follower. A newly allocated identifier is generated if
   * local_sfid identifies a meta follower. If connect fails, @ref Ids::kInvalid will be returned.
   */
  sfid_t connect(sfid_t local_sfid, quiddity::qid_t writer_quid, swid_t writer_swid);

  /**
   * \details Connect a local Shmdata follower with an other Quiddity. The method is automatically
   * finding the first compatible shmdata writer available with the target Quiddity. The compatible
   * shmdata writer id (swid) found by this method is necessarily a non meta swid. After allocating
   * the swid, this will call connect() which will find the right shmpath to then call
   * connect_raw(), which will allocate an sfid from the meta follower if need be.
   *
   * \param local_sfid the Shmdata follower identifier to connect. If it is meta, a new follower
   * will be allocated from it. local_sfid must be allocated by the current Claw
   * \param writer_quid the identifier of the Quiddity hosting the Shmdata writer
   *
   * \return the identifier of the follower. A newly allocated identifier is generated if
   * local_sfid identifies a meta follower. If connect fails, Id::kInvalid will be returned.
   */
  sfid_t connect_quid(sfid_t local_sfid, quiddity::qid_t writer_quid);

  /**
   * \brief Connect with another Quiddity. The method is automatically finding
   * the first compatible shmdata writer available with the target Quiddity.
   * It will then call all the chain of connect_quid() => connect() => connect_raw()
   *
   * \param writer_quid the identifier of the Quiddity hosting the Shmdata writer
   *
   * \return the identifier of the follower. A newly allocated identifier is generated if
   * local_sfid identifies a meta follower. If connect fails, Id::kInvalid will be returned.
   */
  sfid_t try_connect(quiddity::qid_t writer_quid);

  /**
   * \details Connect a local Shmdata follower with a Shmdata identified by its path. In case the
   * sfid is already connected to the shmpath passed to this function, this will return the sfid
   * without changing connection state. In case the sfid is already connected to a different sfid,
   * it will call \ref Claw::disconnect() with the already connected shmpath before connecting with
   * the current shmpath. This will also remove associations from the follower_writer_ids_ and
   * follower_swids_ maps.
   *
   * If this is called with a shmpath that is identical to a shmpath that is
   * already connected to the exact sfid (after meta follower allocation), it
   * will do nothing and return the sfid.
   *
   * If a different shmpath was already connected to the sfid (again, after meta
   * follower allocation), this will attempt to disconnect the connected shmpath and
   * then proceed to the connection. It will return @ref switcher::Ids::kInvalid and abort the
   * connection if the disconnection fails)
   *
   * \param local_sfid the Shmdata follower identifier to connect. If it is meta, a new follower
   * will be allocated from it. local_sfid must be allocated by the current Claw
   *
   * \param shmpath the Shmdata path to connect to
   *
   * \return the identifier of the follower. If anything fails, @ref switcher::Ids::kInvalid will be
   * returned.
   */
  sfid_t connect_raw(sfid_t local_sfid, const std::string& shmpath);

  /**
   * Disconnect a Shmdata follower
   *
   * \param sfid the follower identifier
   *
   * \return true if success, false otherwise
   */
  bool disconnect(sfid_t sfid);

  /**
   * Adds a writer to the forced_writer_shmpaths_
   * this forces the use of that shmpath for this writer
   *
   * \param swid id of the writer
   *
   * \param shmpath shmpath to force
   */
  void add_forced_writer(swid_t swid, std::string shmpath);

  /**
   * removes a writer from the forced_writer_shmpaths_
   *
   * \param swid id of the writer
   */
  void remove_forced_writer(swid_t swid);

  /**
   * Struct representing the data necessary to represent a connection from the point
   * of view of a Writer quiddity's swid.
   */
  struct WriterConnection {
    /**
     * Id of the follower quiddity this writer is connected to. Initialized to UUIDs::kInvalid.
     */
    quiddity::qid_t follower_quiddity_id{UUIDs::kInvalid};
    /**
     * Id of the follower sfid this writer is connected to. Initialized to Ids::kInvalid.
     */
    sfid_t connected_sfid{Ids::kInvalid};
  };

  /**
   * Struct representing the data necessary to represent a connection from the point
   * of view of a Follower quiddity's sfid. In case of a raw shmdata connection, the connected_swid
   * and the writer_quiddity_id fields will be \ref switcher::Ids::kInvalid.
   */
  struct FollowerConnection {
    /**
     * Id of the writer quiddity this follower is connected to. Initialized to UUIds::kInvalid.
     * In case the FollowerConnection represents a connection to a raw shmpath, this will stay
     * as UUIds::kInvalid.
     */
    quiddity::qid_t writer_quiddity_id{UUIDs::kInvalid};
    /**
     * Id of the writer swid this follower is connected to. Initialized to Ids::kInvalid.
     * In case the FollowerConnection represents a connection to a raw shmpath, this will stay
     * as Ids::kInvalid.
     */
    swid_t connected_swid{Ids::kInvalid};
    /**
     * Path of the shmdata socket this FollowerConnection is connected to.
     */
    std::string shmdata_path{};
  };

  /**
   * Returns a copy of the std::vector<WriterConnection> representing all registered connections for
   * a swid
   *
   * \param swid : swid to get registered connections from.
   *
   * \return a vector of \ref Claw::WriterConnection that contains all registered connections. The
   * vector will be empty if no connection is associated to that writer.
   */
  std::vector<WriterConnection> get_registered_writer_connections(swid_t swid);

  /**
   * Get the label of a follower from its identifier
   *
   * \param sfid the follower identifier
   *
   * \return the label, or an empty string if sfid is not found
   */
  std::string get_follower_label(sfid_t sfid) const;

  /**
   * Get list of followers labels
   *
   * \return a vector with labels
   */
  std::vector<std::string> get_follower_labels() const;

  /**
   * Get identifier of a follower from its label
   *
   * \param label the follower label
   *
   * \return the identifier is found, Id::kInvalid otherwise
   */
  sfid_t get_sfid(const std::string& label) const;

  /**
   * Get list of follower identifiers
   *
   * \return the identifiers list
   */
  std::vector<sfid_t> get_sfids() const;

  /**
   * Get list of writers identifier
   *
   * \return the identifiers list
   */
  std::vector<swid_t> get_swids() const;

  /**
   * Get the label of a writer from its identifier
   *
   * \param swid the writer identifier
   *
   * \return the label, or an empty string if swid is not found
   */
  std::string get_writer_label(swid_t swid) const;

  /**
   * Get list of writers labels
   *
   * \return a vector with labels
   */
  std::vector<std::string> get_writer_labels() const;

  /**
   * Get identifier of a writer from its label
   *
   * \param label the writer label
   *
   * \return the identifier is found, Id::kInvalid otherwise
   */
  swid_t get_swid(const std::string& label) const;

  /**
   * Get the Shmdata path used by the writer
   *
   * \param swid the writer identifier
   *
   * \return the path, or empty string if swid is not found
   */
  std::string get_writer_shmpath(swid_t swid) const;

  /**
   * Get the Shmdata path from the writer label
   *
   * \param swid the writer identifier
   *
   * \return the path, or empty string if swid is not found
   */
  std::string get_shmpath_from_writer_label(const std::string& label) const;

  /**
   * Get shmdata follower ids compatible with a given shmdata type
   *
   * \param type the shmdata type
   *
   * \return the vector with all identifiers of compatible shmdata follower
   */
  std::vector<sfid_t> get_compatible_sfids(const ::shmdata::Type& type) const;

  /**
   * Get shmdata writer ids compatible with a given shmdata type
   *
   * \param type the shmdata type
   *
   * \return the vector with all identifiers of compatible shmdata writer
   */
  std::vector<swid_t> get_compatible_swids(const ::shmdata::Type& type) const;

  /**
   * Test if a local shmdata follower is compatible with the shmdata writer of a quiddity
   *
   * \param local_sfid the shmdata follower to test
   * \param writer_quid the writer Quiddity identifier
   * \param writer_swid the writer_quid's shmdata writer identifier
   *
   * \return true if compatible, false otherwise
   */
  bool can_do_swid(sfid_t local_sfid, quiddity::qid_t writer_quid, swid_t writer_swid) const;

  /**
   * Test if a local shmdata follower is compatible with a shmdata type
   *
   * \param local_sfid the shmdata follower to test
   * \param type the shmdata type
   *
   * \return true if compatible, false otherwise
   */
  bool sfid_can_do_shmtype(sfid_t local_sfid, const ::shmdata::Type& type) const;

  /**
   * Test if a local shmdata writer is compatible with a shmdata type
   *
   * \param local_swid the shmdata follower to test
   * \param type the shmdata type
   *
   * \return true if compatible, false otherwise
   */
  bool swid_can_do_shmtype(swid_t local_swid, const ::shmdata::Type& type) const;

  /**
   * Get the list of compatible types with a given shmdata follower
   *
   * \param sfid the shmdata follower identifier
   *
   * \return the list of compatible shmdata types
   */
  std::vector<::shmdata::Type> get_follower_can_do(sfid_t sfid) const;

  /**
   * Get the list of compatible types with a given shmdata writer
   *
   * \param sfid the shmdata writer identifier
   *
   * \return the list of compatible shmdata types
   */
  std::vector<::shmdata::Type> get_writer_can_do(swid_t swid) const;

  /**
   * Gets a copy of the FollowerConnection struct associated to a sfid.
   *
   * \param the sfid we want to get the connection for.
   *
   * \return a const reference to the FollowerConnection struct associated to a sfid.
   * If its not found, returns an empty follower connections with
   * its id type fields initialized to Ids::kInvalid and its std::string field to
   * the empty string.
   */
  FollowerConnection get_follower_connection(sfid_t sfid) const;

  /**
   * Get the shmdata path a follower is connected to
   *
   * \param the shmdata follower identifier
   *
   * \return The shmdata path or empty string if not connected
   */
  std::string get_follower_shmpath(sfid_t sfid) const;

  /**
   * Get the writer swid a follower is associated to
   *
   * \param the shmdata follower identifier
   *
   * \return The swid or @ref switcher::Ids::kInvalid if not associated
   */
  swid_t get_follower_swid(sfid_t sfid) const;

  /**
   * Get the writer quiddity id a follower is associated to
   *
   * \param the shmdata follower identifier
   *
   * \return The writer quiddity id or @ref switcher::UUIDs::kInvalid if not associated
   */
  quiddity::qid_t get_follower_writer_id(sfid_t sfid) const;

  /**
   * Gets the from_sfid of a sfid.
   *
   * \param sfid the sfid of which we want to know the meta parent
   *
   * \return the parent meta sfid from which the sfid comes from or @ref switcher::Ids::kInvalid in
   * case the passed sfid is not meta.
   */
  sfid_t get_meta_sfid(sfid_t sfid) const { return connection_spec_.get_meta_sfid(sfid); }

  /**
   * Add a writer corresponding to a meta writer specification.
   * It updates the connection specification.
   * This method is suposed to be used from Quiddity plugins.
   *
   * \param id the id of the meta Shmdata writer
   * \param spec the writer description (label + description).
   *        if the label field of the argument is is empty, then
   *        the claw will generate one
   * \param forced_writer_shmpath The shmpath that should be forced for the generated writer.
   * if no shmpath should be forced, this should be left to its default value of "". This is so that
   * the forced shmpath is set before the call to on_connection_spec_grafted_
   *
   * \return the new Shmdata writer id
   */
  swid_t add_writer_to_meta(swid_t id,
                            const shm_spec_t& spec,
                            std::string forced_writer_shmpath = "");

  /**
   * Remove a writer from a meta writer specification.
   * It updates the connection specification.
   * This method is suposed to be used from Quiddity plugins.
   *
   * \param id the Shmdata writer to remove
   *
   * \return true is the writer has been removed, false otherwise.
   */
  bool remove_writer_from_meta(swid_t id);

  /**
   * Replace the follower can_do entry.
   *
   * \param sfid the follower identifier
   * \param types the shmdata types to apply
   *
   * \return applied or not
   */
  bool replace_follower_can_do(sfid_t sfid, const std::vector<::shmdata::Type>& types);

  /**
   * Replace the writer can_do entry.
   *
   * \param swid the writer identifier
   * \param types the shmdata types to apply
   *
   * \return applied or not
   */
  bool replace_writer_can_do(swid_t swid, const std::vector<::shmdata::Type>& types);

  /**
   * Gets a shmdata follower identifier (sfid) from its shmpath.
   *
   * \param shmpath the follower shmpath
   *
   * \return sfid if found else Ids::kInvalid
   */
  sfid_t get_follower_sfid(const std::string& shmpath) const;

  /**
   * Gets the number of sfid associated with this particular shmpath.
   *
   * \param shmpath : the followed shmpath
   *
   * \return number of sfid associated with the shmpath
   */
  size_t get_number_of_sfid_for_shmpath(const std::string& shmpath) const;

  /**
   * Creates an InfoTree representing the connection to a particular sfid.
   * The infoTree is of the form
   *   {
   *     "follower":{
   *       "id":<follower quid id>,
   *       "sfid": <sfid>
   *       "from_sfid": <meta follower id | 0 if the sfid is not from a meta follower>
   *     },
   *     "writer":{
   *       "id":<writer quid id>
   *       "swid": <swid>
   *     }
   *     "raw_shmdata":{
   *       "shmdata_path": <the shmpath>
   *     }
   *   }
   * where raw_shmdata and writer are mutually exclusive.
   *
   * This function is used to serialize connection information when emitting connection and
   * disconnection events and when getting the state of the whole system.
   */
  InfoTree::ptr serialize_follower_connection(sfid_t sfid);

 private:
  Quiddity* quid_;
  ConnectionSpec connection_spec_{};
  OnConnect_t on_connect_cb_{};
  OnDisconnect_t on_disconnect_cb_{};

  /**
   * Map of swid => vector of WriterConnection. This represent all the connections that were made
   * from a swid to a sfid of a quiddity. We use this to cleanup connections on removal of a writer
   * quiddity from the switcher's instance. The cleanup is only done on a call to \ref
   * switcher::Container::remove.
   *
   * The association of a swid to a vector of connections is because 1 writer
   * can be connected to many followers
   */
  std::map<swid_t, std::vector<WriterConnection>> writer_connections_{};

  /**
   * This map contains associations of <sfid> -> < \ref Claw::FollowerConnection >.
   * This gets updated on calls to \ref Claw::connect_raw and \ref Claw::connect
   *
   * The association of a sfid to a single connection is because a follower
   * can only be connected to one writer.
   */
  std::map<sfid_t, FollowerConnection> follower_connections_{};

  /**
   * Manages the registration of a connection to the swid of a writer quiddity.
   *
   * \param swid of the writer
   *
   * \param connected_sfid sfid the swid is connected to
   *
   * \param follower_quiddity_id id of the quiddity to which connected_sfid belongs.
   */
  void register_writer_connection(swid_t swid,
                                  sfid_t connected_sfid,
                                  quiddity::qid_t follower_quiddity_id);

  /**
   * Manages the unregistration of a connection to the swid of a writer quiddity
   *
   * \param swid of the writer
   *
   * \param connected_sfid sfid the swid is no longer connected to
   *
   * \param follower_quiddity_id id of the quiddity to which connected_sfid belongs.
   */
  void unregister_writer_connection(swid_t swid,
                                    sfid_t connected_sfid,
                                    quiddity::qid_t follower_quiddity_id);

  /**
   * Finds the index of a writer connection.
   *
   * \param swid of the writer
   *
   * \param connected_sfid sfid the swid of the connection we are looking for
   *
   * \param follower_quiddity_id id of the quiddity to which connected_sfid belongs.
   *
   * \return the index of the found connection or -1 if not found.
   */
  int64_t find_writer_connection_index(swid_t swid,
                                       sfid_t connected_sfid,
                                       quiddity::qid_t follower_quiddity_id);

  /**
   * This is used to override the automatic generation of shmpath name from the switcher quiddity id
   * for certain special quiddities like bundle (which encapsulate many internal quiddities inside
   * of one) and extshmsrc which acts as a proxy for other existing shmpaths.
   *
   * Note : each special case needs to be declared as a friend class in order to have access to
   * this.
   */
  std::map<swid_t, std::string> forced_writer_shmpaths_{};
  /**
   * Implementation of the safe bool idioms. The Claw
   * is considered as valid if the Connection Spec has been
   * parsed with success
   *
   */
  bool safe_bool_idiom() const;
};

}  // namespace claw
}  // namespace quiddity
}  // namespace switcher
#endif
