/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./claw.hpp"

#include "../../switcher.hpp"
#include "../../utils/scope-exit.hpp"
#include "../quiddity.hpp"

namespace switcher {
namespace quiddity {
namespace claw {

bool Claw::safe_bool_idiom() const { return static_cast<bool>(connection_spec_); }

Claw::Claw(Quiddity* quid,
           ConnectionSpec&& spec,
           OnConnect_t on_connect_cb,
           OnDisconnect_t on_disconnect_cb)
    : quid_(quid),
      // we need to move spec because it is not copy constructible because of the unique_ptr in the
      // Ids class.
      connection_spec_(std::move(spec)),
      on_connect_cb_(on_connect_cb),
      on_disconnect_cb_(on_disconnect_cb) {
  // check specification has been parsed
  if (!static_cast<bool>(connection_spec_)) {
    quid_->sw_error("Connection spec error: {}", connection_spec_.msg());
    return;
  }
  // check that callbacks for follower connection are installed properly
  if (!connection_spec_.get_follower_labels().empty()) {
    if (!on_connect_cb_) {
      quid_->sw_warning(
          "a follower is specified in connection spec, but no connection callback is configured");
      return;
    }
    if (!on_disconnect_cb_) {
      quid_->sw_warning(
          "a follower is specified in connection spec, but no disconnection callback is "
          "configured");
      return;
    }
  }
  // sets the conpecs quiddity pointer
  connection_spec_.quid_ = quid_;
  quid_->connection_spec_tree_ = connection_spec_.get_tree();
}

std::string Claw::get_follower_label(sfid_t sfid) const {
  return connection_spec_.get_follower_label(sfid);
}

std::string Claw::get_writer_label(swid_t swid) const {
  return connection_spec_.get_writer_label(swid);
}

sfid_t Claw::get_sfid(const std::string& label) const { return connection_spec_.get_sfid(label); }

swid_t Claw::get_swid(const std::string& label) const { return connection_spec_.get_swid(label); }

sfid_t Claw::connect(sfid_t local_sfid, quiddity::qid_t writer_quid, swid_t writer_swid) {
  auto writer_qrox = quid_->qcontainer_->get_qrox(writer_quid);
  if (!writer_qrox) {
    quid_->sw_warning("Quiddity {} not found, cannot connect", writer_quid);
    return Ids::kInvalid;
  }
  auto shmpath = writer_qrox.get()->claw_.get_writer_shmpath(writer_swid);
  if (shmpath.empty()) {
    quid_->sw_warning("Shmdata writer {} not found, cannot connect", std::to_string(writer_swid));
    return Ids::kInvalid;
  }
  quid_->sw_debug(
      "connecting quiddity {} (follower {}) with quiddity {} (writer {}, shmdata path {})",
      quid_->get_nickname(),
      get_follower_label(local_sfid),
      writer_qrox.get()->get_nickname(),
      writer_qrox.get()->claw_.get_writer_label(writer_swid),
      shmpath);

  // try to connect to the shmpath using connect_raw. return an invalid Id if it doesn't work.
  auto connection_sfid = connect_raw(local_sfid, shmpath);
  if (connection_sfid == Ids::kInvalid) {
    return connection_sfid;
  }
  // saves the association from sfid to writer quiddity id and writer swid
  quid_->sw_debug("associating sfid {} with writer quid id {} and swid {}",
                  connection_sfid,
                  writer_quid,
                  writer_swid);
  follower_connections_.at(connection_sfid).connected_swid = writer_swid;
  follower_connections_.at(connection_sfid).writer_quiddity_id = writer_quid;
  // sets the connection in the writer too.
  writer_qrox.get()->claw_.register_writer_connection(
      writer_swid, connection_sfid, quid_->get_id());
  quid_->smanage<MPtr(&signal::SBag::notify)>(quid_->on_connected_id_,
                                              serialize_follower_connection(connection_sfid));
  return connection_sfid;
}

sfid_t Claw::connect_raw(sfid_t sfid, const std::string& shmpath) {
  auto id = sfid;
  const bool is_meta = connection_spec_.is_meta_follower(sfid);
  // if the sdif has a meta follower connection spec, allocate a new id from the meta follower.
  if (is_meta) {
    id = connection_spec_.allocate_sfid_from_meta(sfid);
  }
  if (!connection_spec_.is_allocated_follower(id)) {
    quid_->sw_warning("cannot connect_raw, shmdata follower not found (id requested is {})",
                      std::to_string(sfid));
    return Ids::kInvalid;
  }
  // looks to see if the sfid is in the map.
  if (follower_connections_.find(id) != follower_connections_.end()) {
    auto existing_shmpath = follower_connections_.at(id).shmdata_path;
    quid_->sw_debug(
        "found an existing connection of shmpath {} to the sfid {} for quiddity of id {}",
        existing_shmpath,
        id,
        quid_->get_id());
    if (existing_shmpath == shmpath) {
      // if our sfid is in the map and our follower shmpaths is the same as the one that was
      // connected to this sfid, this is already an existing connection.
      // Don't connect again and return the sfid
      quid_->sw_warning(
          "sfid {} was already connected to shmpath {} for quiddity of id {}, returning it without "
          "changing connection state",
          id,
          shmpath,
          quid_->get_id());
      return id;
    }
    // if there was already a shmdata there and it is not the same as the one we want to connect,
    // disconnect it before connecting the new shmdata. This is because at that point, our sfid
    // should only describe a connection that is valid for a single shmdata since the meta sfid
    // allocation step is done.
    quid_->sw_warning("disconnecting shmpath {} from sfid {} for quiddity of id {}",
                      id,
                      existing_shmpath,
                      quid_->get_id());
    if (!disconnect(id)) {
      quid_->sw_warning(
          "failed to disconnect shmpath {} from sfid {} while trying to connect new shmpath {}, "
          "stopping connection",
          existing_shmpath,
          sfid,
          shmpath);
      return Ids::kInvalid;
    }
  }
  // we emplace the association of sfid -> shmpath here to be able to
  // to expose the sfid of the connection in its infotree.
  FollowerConnection connection;
  connection.shmdata_path = shmpath;
  follower_connections_.emplace(id, connection);

  // trigger quiddity callback. Cleanup if it fails.
  if (!(on_connect_cb_(shmpath, id))) {
    // this will leave the association of sfid -> FollowerConnection in follower_connections_
    // even if the function returns that the connection was invalid. We should maybe
    // take additionnal steps to cleanup this value.
    // See : https://gitlab.com/sat-mtl/tools/switcher/-/issues/186
    if (is_meta) {
      connection_spec_.deallocate_sfid_from_meta(id);
    }
    quid_->sw_warning("connect callback failed");
    // remove association from sfid to shmpath since the connection failed.

    auto it = follower_connections_.find(id);
    if (follower_connections_.end() != it) {
      follower_connections_.erase(it);
    }
    return Ids::kInvalid;
  }

  // notify new follower
  if (is_meta) {
    quid_->smanage<MPtr(&signal::SBag::notify)>(quid_->on_connection_spec_grafted_id_,
                                                InfoTree::make("follower." + std::to_string(id)));
  }
  quid_->sw_debug("successfully connected shmpath {} to sfid {} for quiddity of id {}",
                  shmpath,
                  id,
                  quid_->get_id());

  return id;
}

bool Claw::disconnect(sfid_t sfid) {
  if (!connection_spec_.is_allocated_follower(sfid)) {
    quid_->sw_warning("cannot disconnect (sfid {} not allocated)", std::to_string(sfid));
    return false;
  }
  if (get_follower_shmpath(sfid).empty()) {
    quid_->sw_warning("cannot disconnect (sfid {} not connected to anything)",
                      std::to_string(sfid));
    return false;
  }
  if (!(on_disconnect_cb_(sfid))) {
    return false;
  }
  // remove from connection spec if a follower is generated from a meta follower
  if (connection_spec_.deallocate_sfid_from_meta(sfid)) {
    quid_->smanage<MPtr(&signal::SBag::notify)>(quid_->on_connection_spec_pruned_id_,
                                                InfoTree::make("follower." + std::to_string(sfid)));
  }
  // unregister this connection from the writer if the writer exists (i.e. this is not a connection
  // to a raw shmdata)
  quiddity::qid_t writer_quid_id = get_follower_writer_id(sfid);
  swid_t connected_swid = get_follower_swid(sfid);
  if (writer_quid_id != UUIDs::kInvalid) {
    auto writer_qrox = quid_->qcontainer_->get_qrox(writer_quid_id);
    if (writer_qrox) {
      writer_qrox.get()->claw_.unregister_writer_connection(connected_swid, sfid, quid_->get_id());
    } else {
      // Even if we cant unregister the connection in the writer quiddity, we should still carry
      // on and unregister it in the follower.
      quid_->sw_warning("Quiddity {} not found, cannot unregister connection", writer_quid_id);
    }
  }
  // notify disconnected with the deleted connection as data.
  quid_->smanage<MPtr(&signal::SBag::notify)>(quid_->on_disconnected_id_,
                                              serialize_follower_connection(sfid));
  // unregister the connection from the follower.
  auto it = follower_connections_.find(sfid);
  if (follower_connections_.end() != it) {
    follower_connections_.erase(it);
  }
  return true;
}

std::vector<Claw::WriterConnection> Claw::get_registered_writer_connections(swid_t swid) {
  try {
    return writer_connections_.at(swid);
  } catch (std::out_of_range& ex) {
    return std::vector<Claw::WriterConnection>();
  }
}

InfoTree::ptr Claw::serialize_follower_connection(sfid_t sfid) {
  InfoTree::ptr connection_tree = InfoTree::make();
  std::string attached_shmpath = get_follower_shmpath(sfid);
  // if we receive an empty string as the shmpath, this sfid is not connected.
  if (attached_shmpath.empty()) {
    return connection_tree;
  }
  // gets the meta sfid the sfid originates from. If its 0, this is not a sfid created
  // from a meta sfid.
  auto from_sfid = get_meta_sfid(sfid);
  connection_tree->graft(".follower.id", InfoTree::make(quid_->get_id()));
  connection_tree->graft(".follower.from_sfid", InfoTree::make(from_sfid));
  connection_tree->graft(".follower.sfid", InfoTree::make(sfid));
  // gets the id of the writer quiddity associated to the current sfid.
  auto writer_quid_id = get_follower_writer_id(sfid);
  if (writer_quid_id == UUIDs::kInvalid) {
    // if there is no quiddity associated to the current sfid, this is a connection to a raw
    // shmpath.
    connection_tree->graft(".raw_shmdata.shmdata_path", InfoTree::make(attached_shmpath));
    return connection_tree;
  }
  // this will not mean much reload if the writer swid is a dynamic swid that needs some specific
  // user (or program) interaction before it is instanciated.
  auto swid = get_follower_swid(sfid);

  connection_tree->graft(".writer.id", InfoTree::make(writer_quid_id));
  connection_tree->graft(".writer.swid", InfoTree::make(swid));
  return connection_tree;
}

std::string Claw::get_writer_shmpath(swid_t id) const {
  if (!connection_spec_.is_allocated_writer(id)) {
    quid_->sw_warning("cannot provide shmpath for id {} (not allocated)", std::to_string(id));
    return "";
  }
  if (connection_spec_.is_meta_writer(id)) {
    quid_->sw_warning("cannot provide shmpath for id {} (id refers to a meta Shmdata)",
                      std::to_string(id));
    return "";
  }
  // check if shmpath is overriden
  const auto& overriden = forced_writer_shmpaths_.find(id);
  if (overriden != forced_writer_shmpaths_.cend()) {
    return overriden->second;
  }
  // otherwise generate to shmpath
  auto switcher = this->quid_->qcontainer_->get_switcher();
  std::string basepath = (fs::path(switcher->get_shm_dir()) / switcher->get_shm_prefix()).string();
  return basepath + switcher->name_ + "_" + quid_->id_ + "_" + std::to_string(id);
}

std::string Claw::get_shmpath_from_writer_label(const std::string& label) const {
  return get_writer_shmpath(get_swid(label));
}

swid_t Claw::add_writer_to_meta(swid_t swid,
                                const shm_spec_t& spec,
                                std::string forced_writer_shmpath) {
  auto id = connection_spec_.allocate_swid_from_meta(swid, spec);
  if (Ids::kInvalid == id) {
    return id;
  }
  if (!forced_writer_shmpath.empty()) {
    add_forced_writer(id, forced_writer_shmpath);
  }

  quid_->smanage<MPtr(&signal::SBag::notify)>(quid_->on_connection_spec_grafted_id_,
                                              InfoTree::make("writer." + std::to_string(id)));
  return id;
}

void Claw::add_forced_writer(swid_t swid, std::string shmpath) {
  forced_writer_shmpaths_.emplace(swid, shmpath);
}

void Claw::remove_forced_writer(swid_t swid) {
  auto forced_writer_it = forced_writer_shmpaths_.find(swid);
  if (forced_writer_it == forced_writer_shmpaths_.end()) {
    return;
  }
  forced_writer_shmpaths_.erase(forced_writer_it);
}

bool Claw::remove_writer_from_meta(swid_t swid) {
  auto res = connection_spec_.deallocate_swid_from_meta(swid);
  if (res) {
    quid_->smanage<MPtr(&signal::SBag::notify)>(quid_->on_connection_spec_pruned_id_,
                                                InfoTree::make("writer." + std::to_string(swid)));
  }
  return res;
}

std::vector<std::string> Claw::get_follower_labels() const {
  return connection_spec_.get_follower_labels();
}

std::vector<std::string> Claw::get_writer_labels() const {
  return connection_spec_.get_writer_labels();
}

bool Claw::sfid_can_do_shmtype(sfid_t sfid, const ::shmdata::Type& shmtype) const {
  if (shmtype.name() == "all") {
    return true;
  }
  const auto& can_dos = connection_spec_.get_follower_can_do(sfid);
  auto writer_caps = gst_caps_from_string(shmtype.str().c_str());
  On_scope_exit { gst_caps_unref(writer_caps); };
  for (const auto& can_do : can_dos) {
    const auto& can_do_str = can_do.str();
    if (can_do_str == "all") {
      return true;
    }
    auto can_do_caps = gst_caps_from_string(can_do_str.c_str());
    On_scope_exit { gst_caps_unref(can_do_caps); };
    if (gst_caps_can_intersect(can_do_caps, writer_caps)) {
      return true;
    }
  }
  return false;
}

bool Claw::swid_can_do_shmtype(swid_t swid, const ::shmdata::Type& shmtype) const {
  if (shmtype.name() == "all") {
    return true;
  }
  const auto& can_dos = connection_spec_.get_writer_can_do(swid);
  auto shmtype_caps = gst_caps_from_string(shmtype.str().c_str());
  On_scope_exit { gst_caps_unref(shmtype_caps); };
  for (const auto& can_do : can_dos) {
    const auto& can_do_str = can_do.str();
    if (can_do_str == "all") {
      return true;
    }
    auto can_do_caps = gst_caps_from_string(can_do_str.c_str());
    On_scope_exit { gst_caps_unref(can_do_caps); };
    if (gst_caps_can_intersect(can_do_caps, shmtype_caps)) {
      return true;
    }
  }
  return false;
}

bool Claw::can_do_swid(sfid_t local_sfid, quiddity::qid_t writer_quid, swid_t writer_swid) const {
  auto writer_qrox = quid_->qcontainer_->get_qrox(writer_quid);
  if (!writer_qrox) {
    quid_->sw_warning("Quiddity {} not found", writer_quid);
    return false;
  }
  auto writer_can_do = writer_qrox.get()->claw_.get_writer_can_do(writer_swid);
  for (const auto& can_do : writer_can_do) {
    if (sfid_can_do_shmtype(local_sfid, can_do)) {
      return true;
    }
  }
  return false;
}

std::vector<sfid_t> Claw::get_compatible_sfids(const ::shmdata::Type& type) const {
  std::vector<sfid_t> res;
  for (const auto& sfid : connection_spec_.get_follower_sfids()) {
    if (sfid_can_do_shmtype(sfid, type)) {
      res.push_back(sfid);
    }
  }
  return res;
}

std::vector<swid_t> Claw::get_compatible_swids(const ::shmdata::Type& type) const {
  std::vector<swid_t> res;
  for (const auto& swid : connection_spec_.get_writer_swids()) {
    if (swid_can_do_shmtype(swid, type) && !connection_spec_.is_meta_writer(swid)) {
      res.push_back(swid);
    }
  }
  return res;
}

std::vector<::shmdata::Type> Claw::get_follower_can_do(sfid_t sfid) const {
  return connection_spec_.get_follower_can_do(sfid);
}

std::vector<::shmdata::Type> Claw::get_writer_can_do(swid_t swid) const {
  return connection_spec_.get_writer_can_do(swid);
}

sfid_t Claw::connect_quid(sfid_t local_sfid, quiddity::qid_t writer_quid) {
  auto writer_qrox = quid_->qcontainer_->get_qrox(writer_quid);
  if (!writer_qrox) {
    quid_->sw_warning("Quiddity {} not found, cannot connect", writer_quid);
    return Ids::kInvalid;
  }
  swid_t swid = Ids::kInvalid;
  for (const auto& can_do : get_follower_can_do(local_sfid)) {
    auto swids = writer_qrox.get()->claw_.get_compatible_swids(can_do);
    if (!swids.empty()) {
      swid = swids[0];
      break;
    }
  }
  if (Ids::kInvalid == swid) {
    quid_->sw_debug("No compatible shmdata found, cannot connect {} ({}) with {}",
                    quid_->get_nickname(),
                    quid_->claw_.get_follower_label(local_sfid),
                    writer_qrox.get()->get_nickname());
    return Ids::kInvalid;
  }
  return connect(local_sfid, writer_quid, swid);
}

sfid_t Claw::try_connect(quiddity::qid_t writer_quid) {
  auto res = Ids::kInvalid;
  for (const auto& sfid : get_sfids()) {
    res = connect_quid(sfid, writer_quid);
    if (Ids::kInvalid != res) {
      return res;
    }
  }
  return res;
}

bool Claw::replace_follower_can_do(sfid_t sfid, const std::vector<::shmdata::Type>& types) {
  return connection_spec_.replace_follower_can_do(sfid, types);
}

bool Claw::replace_writer_can_do(swid_t swid, const std::vector<::shmdata::Type>& types) {
  return connection_spec_.replace_writer_can_do(swid, types);
}

Claw::FollowerConnection Claw::get_follower_connection(sfid_t sfid) const {
  const auto found = follower_connections_.find(sfid);
  if (found == follower_connections_.cend()) {
    return FollowerConnection{};
  }
  return found->second;
}

std::string Claw::get_follower_shmpath(sfid_t sfid) const {
  return get_follower_connection(sfid).shmdata_path;
}

swid_t Claw::get_follower_swid(sfid_t sfid) const {
  return get_follower_connection(sfid).connected_swid;
}

quiddity::qid_t Claw::get_follower_writer_id(sfid_t sfid) const {
  return get_follower_connection(sfid).writer_quiddity_id;
}

sfid_t Claw::get_follower_sfid(const std::string& shmpath) const {
  for (const auto& item : follower_connections_) {
    if (item.second.shmdata_path == shmpath) {
      return item.first;
    }
  }
  return Ids::kInvalid;
}

size_t Claw::get_number_of_sfid_for_shmpath(const std::string& shmpath) const {
  size_t count = 0;
  for (const auto& item : follower_connections_) {
    if (item.second.shmdata_path == shmpath) {
      count++;
    }
  }
  return count;
}

void Claw::register_writer_connection(swid_t swid,
                                      sfid_t connected_sfid,
                                      quiddity::qid_t follower_quiddity_id) {
  auto connection_index = find_writer_connection_index(swid, connected_sfid, follower_quiddity_id);
  if (connection_index != -1) {
    quid_->sw_warning(
        "Cannot register a registered connection. writer quidditiy id : {} swid : {} "
        "connected_sfid : {} follower_quiddity_id : {}",
        quid_->get_id(),
        swid,
        connected_sfid,
        follower_quiddity_id);
    return;
  }
  WriterConnection writer_connection;
  writer_connection.follower_quiddity_id = follower_quiddity_id;
  writer_connection.connected_sfid = connected_sfid;
  writer_connections_[swid].push_back(writer_connection);
}

void Claw::unregister_writer_connection(swid_t swid,
                                        sfid_t connected_sfid,
                                        quiddity::qid_t follower_quiddity_id) {
  auto connection_index = find_writer_connection_index(swid, connected_sfid, follower_quiddity_id);
  if (connection_index == -1) {
    quid_->sw_warning(
        "Cannot unregister an unregistered connection. writer quidditiy id : {} swid : {} "
        "connected_sfid : {} follower_quiddity_id : {}",
        quid_->get_id(),
        swid,
        connected_sfid,
        follower_quiddity_id);
    return;
  }
  // This could throw an exception in theory but at that point in the function
  // we know there is a vector at [swid] and we know there is something at begin() + connected_index
  writer_connections_[swid].erase(writer_connections_[swid].begin() + connection_index);
}

int64_t Claw::find_writer_connection_index(swid_t swid,
                                           sfid_t connected_sfid,
                                           quiddity::qid_t follower_quiddity_id) {
  // if we don't even have an entry in the map, return -1
  if (writer_connections_.find(swid) == writer_connections_.end()) {
    return -1;
  }
  std::vector<WriterConnection>& swid_connections = writer_connections_[swid];
  // find the connection in the vector
  // we need a counter variable here and if we don't declare an unsigned type, the compiler
  // doesn't want to do the comparison. Might as well use the real return type of
  // swid_connections.size()
  for (std::vector<WriterConnection>::size_type i = 0; i < swid_connections.size(); ++i) {
    if (connected_sfid == swid_connections[i].connected_sfid &&
        follower_quiddity_id == swid_connections[i].follower_quiddity_id) {
      return i;
    }
  }
  // if not found, return -1
  return -1;
}

std::vector<sfid_t> Claw::get_sfids() const { return connection_spec_.get_follower_sfids(); }

std::vector<swid_t> Claw::get_swids() const { return connection_spec_.get_writer_swids(); }

}  // namespace claw
}  // namespace quiddity
}  // namespace switcher
