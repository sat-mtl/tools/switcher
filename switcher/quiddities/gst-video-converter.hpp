/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_GST_VIDEO_CONVERTER_H__
#define __SWITCHER_GST_VIDEO_CONVERTER_H__

#include <memory>

#include "../gst/pixel-format-converter.hpp"
#include "../quiddity/quiddity.hpp"
#include "../shmdata/gst-tree-updater.hpp"

namespace switcher {
namespace quiddities {
using namespace quiddity;

/**
 * The GstVideoConverter class, it wraps gstreamer in order to convert any video stream into a
 *specific pixel format.
 **/
class GstVideoConverter : public Quiddity {
 public:
  GstVideoConverter(quiddity::Config&&);

 private:
  static const std::string kConnectionSpec;  //!< Shmdata specifications
  std::string shmpath_to_convert_{};
  std::string shmpath_converted_{};
  property::Selection<> video_format_;
  property::prop_id_t video_format_id_;
  std::unique_ptr<shmdata::GstTreeUpdater> shmsrc_sub_{nullptr};
  std::unique_ptr<shmdata::GstTreeUpdater> shmsink_sub_{nullptr};
  std::unique_ptr<gst::PixelFormatConverter> converter_{nullptr};

  /**
     This calls the unique_ptr.reset() with an empty argument on the two GstTreeUpdaters created in
     on_shmdata_connect(), replacing them with nullptr and destroying them in the process. It also
     seems to do some more ressource management, calling reset on its gst element. \return A boolean
     asserting that a shmdata is disconnected. A false value is aborting the disconnection.
  */
  bool on_shmdata_disconnect();

  /**
   * This is called when a connection is made to this quiddity. Does some validation and creates two
   GstTreeUpdater, one for the input and one for the output. It also creates a gst element to do the
   conversion
   * \param shmdata_socket_path path of the shmdata.
   * \param sfid sfid of the connection.
   * \return A boolean asserting that a shmdata is connected. A false value is aborting the
   connection.
   */
  bool on_shmdata_connect(const std::string& shmdata_socket_path, claw::sfid_t sfid);
};

}  // namespace quiddities
}  // namespace switcher
#endif
