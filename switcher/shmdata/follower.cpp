/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./follower.hpp"

#include "./caps/utils.hpp"

namespace switcher {
namespace shmdata {

Follower::Follower(quiddity::Quiddity* quid,
                   const std::string& path,
                   ::shmdata::Reader::onData od,
                   ::shmdata::Reader::onServerConnected osc,
                   ::shmdata::Reader::onServerDisconnected osd,
                   std::chrono::milliseconds update_interval,
                   Direction dir,
                   bool get_shmdata_on_connect)
    : quid_(quid),
      shmlogger_(quid),
      od_(od),
      osc_(osc),
      osd_(osd),
      tree_path_(dir == Direction::reader ? ".shmdata.reader." + path : ".shmdata.writer." + path),
      dir_(dir),
      get_shmdata_on_connect_(get_shmdata_on_connect),
      follower_(std::make_unique<::shmdata::Follower>(
          path,
          [this](void* data, size_t size) { this->on_data(data, size); },
          [this, path, quid](const std::string& data_type) {
            this->on_server_connected(
                data_type, quid->claw<MPtr(&quiddity::claw::Claw::get_follower_sfid)>(path));
          },
          [this]() { this->on_server_disconnected(); },
          &shmlogger_)),
      task_(std::make_unique<PeriodicTask<>>([this]() { this->update_quid_stats(); },
                                             update_interval)) {
  // adding default informations for this shmdata (if its not already there)
  // the reason why the information for this shmdata can already be there
  // is that the osc callback passed to follower_ can be called before we get to
  // the initialization. If the tree is not null, we simply do not initialize the tree.
  if (!get_shmdata_on_connect_ && !quid->get_tree(tree_path_)) {
    initialize_tree(tree_path_);
  }
}

Follower::~Follower() {
  follower_.reset(nullptr);
  if (!data_type_.empty()) quid_->prune_tree(tree_path_);
}

void Follower::on_data(void* data, size_t size) {
  {
    std::unique_lock<std::mutex> lock(bytes_mutex_);
    shm_stat_.count_buffer(size);
  }
  if (!od_) return;
  od_(data, size);
}

void Follower::on_server_connected(const std::string& data_type, quiddity::claw::sfid_t sfid) {
  if (get_shmdata_on_connect_) {
    initialize_tree(tree_path_);
  }
  if (data_type != data_type_) {
    data_type_ = data_type;
    quid_->graft_tree(tree_path_ + ".caps", InfoTree::make(data_type_), false);
    // only graft sfid if we are a reader follower.
    if (dir_ == Direction::reader) {
      // sfid changes are important enough to notify
      quid_->graft_tree(tree_path_ + ".sfid", InfoTree::make(sfid), true);
    }
    quid_->graft_tree(
        tree_path_ + ".category", InfoTree::make(caps::get_category(data_type_)), false);
    quid_->notify_tree_updated(tree_path_);
  }
  if (osc_) osc_(data_type_);
}

void Follower::on_server_disconnected() {
  if (osd_) osd_();
}

void Follower::update_quid_stats() {
  std::unique_lock<std::mutex> lock(bytes_mutex_);

  // technically we could avoid sending notification on stat updates for Followers with the reader
  // direction but it makes it easier for scenic to know everything. This also fixes the theoretical
  // issue of getting stat updates for external shmdatas
  Stat::make_tree_updater(quid_, tree_path_)(shm_stat_);
  shm_stat_.reset();
}

void Follower::initialize_tree(const std::string& tree_path) {
  // prunes the tree without sending a signal and gets the pruned tree
  auto tree = quid_->prune_tree(tree_path, false);
  // graft the default empty tree
  quid_->graft_tree(tree_path, quiddity::Quiddity::get_shm_information_template(dir_), false);
  // if what we pruned initially was not null, gets all the sub elements and
  // graft them.
  if (tree) {
    for (auto& it : tree->get_child_keys(".")) {
      // using prune there gradually deletes the values in the original pruned tree
      // we could use get_tree here and it would do the same thing.

      // note : this will break if we introduce nested values in shmdata trees.
      auto value = tree->prune(it);
      quid_->graft_tree(tree_path_ + "." + it, value, false);
    }
  }
}
}  // namespace shmdata
}  // namespace switcher
