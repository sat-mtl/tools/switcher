/*
 * This file is part of libswitcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "./switcher.hpp"

#include <filesystem>
#include <fstream>
#include <string>

#include "./gst/utils.hpp"
#include "./infotree/json-serializer.hpp"
#include "./quiddity/bundle/bundle.hpp"
#include "./quiddity/bundle/description-parser.hpp"
#include "./session/session.hpp"
#include "./utils/file-utils.hpp"
#include "./utils/scope-exit.hpp"

namespace switcher {

namespace fs = std::filesystem;

Switcher::ptr Switcher::make_switcher(const std::string& name, bool debug) {
  Switcher::ptr switcher(new Switcher(name, debug));
  switcher->me_ = switcher;
  return switcher;
}

Switcher::Switcher(const std::string& switcher_name, bool debug)
    : Configurable([&]() { register_bundle_from_configuration(); }),
      Logger(switcher_name, &conf_, debug),
      name_(std::regex_replace(switcher_name, std::regex("[^[:alnum:]| ]"), "-")),
      qfactory_(this),
      qcontainer_(quiddity::Container::make_container(this, &qfactory_)) {
  sw_info("Switcher::Switcher: Initializing Switcher \"{}\"", name_);

  if (debug) {
    sw_debug("Switcher::Switcher: Debug logging enabled");
  }

  sw_info("Switcher::Switcher: Switcher version {}", get_switcher_version());

  sw_info("Switcher::Switcher: GStreamer version {}", gst::Initialized::get_gstreamer_version());

  // shares a pointer to the container as the public quiddities member
  // this is temporary, until we have the time to completely remove reference to qcontainer_
  // or the quids delegate in the code.
  quiddities = qcontainer_.get();
  // intialize default config tree from the `global configuration file`
  const fs::path global_config_file_path = Configuration::get_default_global_path();

  // init configuration from file
  conf_.from_file(global_config_file_path);

  // GStreamer configuration
  apply_gst_configuration();

  // loading bundles from the configuration
  register_bundle_from_configuration();

  // remove shmdata zombies
  remove_shm_zombies();

  // start session
  session_ = std::make_shared<Session>(this);

  // loading plugin located in the SWITCHER_PLUGIN_PATH environment variable
  const auto path = std::getenv("SWITCHER_PLUGIN_PATH");
  if (path)
    for (const auto& it : stringutils::split_string(path, ":")) scan_dir_for_plugins(it);

  // loading plugin from the default plugin directory
  scan_dir_for_plugins(this->qfactory_.get_default_plugin_dir());
}

std::string Switcher::get_switcher_version() const { return SWITCHER_VERSION_STRING; }
void Switcher::set_control_port(const int port) { control_port_ = port; }

int Switcher::get_control_port() const { return control_port_; }

std::string Switcher::get_switcher_caps() const {
  auto caps_str = "switcher-name=(string)" + name_;
  if (get_control_port() > 0) {
    caps_str += ",control-port=(int)" + std::to_string(get_control_port());
  }

  return caps_str;
}

void Switcher::stop() {
  // removes all quiddities
  sw_info("Switcher::stop: removing all quiditties...");
  for (auto& quid : qcontainer_->get_ids()) {
    qcontainer_->remove(quid);
  }
  sw_info("Switcher::stop: stopped.");
}

void Switcher::reset_state() {
  for (auto& quid : qcontainer_->get_ids()) {
    // removes the quiddity if it wasn't in the protect_quiddities_ set
    if (protected_quiddities_.end() == protected_quiddities_.find(quid)) {
      qcontainer_->remove(quid);
    }
  }
}

void Switcher::protect_quiddities() { protect_quiddities(qcontainer_->get_ids()); }

void Switcher::protect_quiddities(std::vector<quiddity::qid_t> protected_ids) {
  // extends the protected_quiddities_ unordered_set with the vector.
  protected_quiddities_.insert(protected_ids.begin(), protected_ids.end());
}

bool Switcher::load_state(InfoTree* state) {
  if (!state) return false;
  // trees
  auto quiddities_user_data = state->get_tree("userdata.");
  auto quiddities = state->get_tree(".quiddities");
  auto properties = state->get_tree(".properties");
  auto connections = state->get_tree(".connections");
  auto custom_states = state->get_tree(".custom_states");

  // making quiddities
  if (quiddities) {
    auto quid_ids = quiddities->get_child_keys(".");
    // creating quiddities
    for (auto& quid_id : quid_ids) {
      std::string quid_kind = quiddities->branch_get_value(quid_id + ".kind");
      std::string quid_nickname = quiddities->branch_get_value(quid_id + ".nickname");
      // Recreates the quiddity with the same uuid as it had.
      auto created = qcontainer_->create(quid_kind, quid_nickname, nullptr, quid_id);
      if (!created) {
        sw_warning("error creating quiddity {} (kind: {}, id: {}): ",
                   quid_nickname,
                   quid_kind,
                   quid_id,
                   created.msg());
      }
      sw_debug("created quiddity {} (kind {}, id {}): ",
               quid_nickname,
               quid_kind,
               quid_id,
               created.msg());
    }

    // loading custom state
    for (auto& quid_id : quid_ids) {
      if (custom_states && !custom_states->empty()) {
        qcontainer_->get_qrox(quid_id).get()->on_loading(custom_states->get_tree(quid_id));
      } else {
        qcontainer_->get_qrox(quid_id).get()->on_loading(InfoTree::make());
      }
    }
  }

  // applying properties
  std::vector<quiddity::qid_t> quid_to_start;
  if (properties) {
    auto quids = properties->get_child_keys(".");
    for (auto& quid_id : quids) {
      auto props = properties->get_child_keys(quid_id);
      for (auto& prop : props) {
        if (prop == "started" && properties->branch_get_value(quid_id + ".started")) {
          quid_to_start.push_back(quid_id);
        } else {
          auto quid = qcontainer_->get_qrox(quid_id).get();
          if (!quid ||
              !quid->prop<MPtr(&quiddity::property::PBag::set_str_str)>(
                  prop, Any::to_string(properties->branch_get_value(quid_id + "." + prop)))) {
            sw_warning("failed to apply value, quiddity is {}, property is {}, value is {}",
                       quid_id,
                       prop,
                       Any::to_string(properties->branch_get_value(quid_id + "." + prop)));
          }
        }
      }
    }
  }

  // applying user data to quiddities
  if (quiddities_user_data) {
    auto quids = quiddities_user_data->get_child_keys(".");
    for (auto& quid_id : quids) {
      auto child_keys = quiddities_user_data->get_child_keys(quid_id);
      for (auto& kit : child_keys) {
        qcontainer_->user_data<MPtr(&InfoTree::graft)>(
            quid_id, kit, quiddities_user_data->get_tree(quid_id + "." + kit));
      }
    }
  }
  // starting quiddities. I think we're doing this after all the properties and
  // before any connections because a lot of quiddity kinds (E.g Executor) do some funky stuff that
  // depends on other properties on their start property being set. Some quiddities also have
  // properties that cannot be changed once they are started
  for (auto& quid_id : quid_to_start) {
    auto quid = qcontainer_->get_qrox(quid_id).get();
    if (!quid) {
      sw_warning("failed to get quiddity {}", quid_id);
      continue;
    }
    if (!quid->prop<MPtr(&quiddity::property::PBag::set_str_str)>("started", "true")) {
      sw_warning("failed to start quiddity {}", quid->get_nickname());
    }
  }

  // restore connection states
  if (connections) {
    auto connection_nodes = connections->get_child_keys(".");
    for (auto& connection_node_key : connection_nodes) {
      auto connection_node = connections->get_tree(connection_node_key);
      // gets the follower quid and the saved connection fields.
      quiddity::qid_t follower_id = connection_node->branch_get_value(".follower.id");
      quiddity::Quiddity* follower_quid = qcontainer_->get_qrox(follower_id).get();
      quiddity::claw::sfid_t sfid =
          connection_node->branch_get_value(".follower.sfid").copy_as<size_t>();
      quiddity::claw::sfid_t from_sfid =
          connection_node->branch_get_value(".follower.from_sfid").copy_as<size_t>();
      // we want to connect to the parent meta sfid of the sfid if it exists. Else
      // connect to the normal sfid.
      quiddity::claw::sfid_t connection_sfid = from_sfid == Ids::kInvalid ? sfid : from_sfid;
      std::string raw_shmpath = connection_node->branch_get_value(".raw_shmdata.shmdata_path");
      if (!raw_shmpath.empty()) {
        // in case we have a raw_shmdata (a shmdata that was created outside the context of the
        // switcher that saved the state), call a connect_raw and carry on with the next connection.
        sw_debug(
            "restored a connection of sfid {} of quiddity with nickname {} with raw shmpath {}",
            sfid,
            follower_quid->get_nickname(),
            raw_shmpath);
        follower_quid->claw<MPtr(&quiddity::claw::Claw::connect_raw)>(connection_sfid,
                                                                      Any::to_string(raw_shmpath));
        continue;
      }
      // otherwise we got a connection to a quiddity, get it.
      std::string writer_id = connection_node->branch_get_value(".writer.id");

      auto writer_qrox = qcontainer_->get_qrox(writer_id);
      if (!writer_qrox) {
        sw_warning(
            "Writer quiddity not found : Cannot restore the connection of follower quiddity {} "
            "with writer quiddity with id {}.",
            follower_quid->get_nickname(),
            writer_id);
        continue;
      }
      auto writer_quid = writer_qrox.get();
      quiddity::claw::swid_t swid =
          connection_node->branch_get_value(".writer.swid").copy_as<size_t>();
      // verify that our swid exists
      if (writer_quid->claw<MPtr(&quiddity::claw::Claw::get_writer_shmpath)>(swid).empty()) {
        // if our swid has an empty string as a writer shmpath, it was probably a dynamic swid
        // that needs to be allocated through a method call or some other event. Skip it.
        sw_warning(
            "Trying to restore connecting from unallocated swid '{}' of writer quiddity with "
            "nickname {} to follower quiddity with nickname {} ",
            swid,
            writer_quid->get_nickname(),
            follower_quid->get_nickname());
        continue;
      }
      follower_quid->claw<MPtr(&quiddity::claw::Claw::connect)>(connection_sfid, writer_id, swid);
      sw_debug(
          "restored a connection of sfid {} of quiddity with nickname {} to swid {} of quiddity "
          "with nickname {}",
          sfid,
          follower_quid->get_nickname(),
          writer_quid->get_nickname(),
          swid);
    }
  }

  // on_loaded
  if (quiddities) {
    auto quid_ids = quiddities->get_child_keys(".");
    for (auto& quid_id : quid_ids) {
      qcontainer_->get_qrox(quid_id).get()->on_loaded();
    }
  }
  return true;
}

InfoTree::ptr Switcher::get_connection_state() const {
  auto quiddities = qcontainer_->get_ids();
  InfoTree::ptr connection_tree = InfoTree::make();
  connection_tree->tag_as_array(".", true);

  // we use this int as an arbitrairy unique key in the connection tree.
  int connection_id = 0;
  for (auto& reader_quid_id : quiddities) {
    auto reader_quid = qcontainer_->get_qrox(reader_quid_id).get();
    auto sfids = reader_quid->claw<MPtr(&quiddity::claw::Claw::get_sfids)>();
    // this will add an item to the the connection list for every connected sfid this quiddity has
    for (auto& sfid : sfids) {
      // gets an arbitrary number as a string for the tree's key.
      std::string current_connection_id = std::to_string(connection_id);
      // increment the connection_id for the next pass.
      ++connection_id;
      InfoTree::ptr connection =
          reader_quid->claw<MPtr(&quiddity::claw::Claw::serialize_follower_connection)>(sfid);
      if (!connection->empty()) {
        connection_tree->graft("." + current_connection_id, connection);
      }
    }
  }
  return connection_tree;
}

InfoTree::ptr Switcher::get_state() const {
  auto quid_ids = qcontainer_->get_ids();
  InfoTree::ptr state_tree = InfoTree::make();

  // saving per-quiddity information
  for (auto& quid_id : quid_ids) {
    auto quid = qcontainer_->get_qrox(quid_id).get();

    auto nick = qcontainer_->get_nickname(quid_id);
    // saving custom tree if some is provided
    auto custom_tree = quid->on_saving();
    if (custom_tree && !custom_tree->empty()) {
      state_tree->graft(".custom_states." + quid_id, std::move(custom_tree));
    }

    // nickname and kind (informations necessary to recreate the quiddities)
    // creates a subtree of the format
    // <some_quiddity_id> : {"nickname" : <some_nickname>, "kind" : <some_kind>}
    // under the "quiddities" subtree.
    if (protected_quiddities_.end() == protected_quiddities_.find(quid_id)) {
      // do not save the protected quiddities. We assume that they will be there with the same uuid
      // when this get loaded This can cause issues, see
      // https://gitlab.com/sat-mtl/tools/switcher/-/issues/214
      InfoTree::ptr creation_information_tree = InfoTree::make();
      creation_information_tree->graft(".nickname", InfoTree::make(nick));
      creation_information_tree->graft(".kind", InfoTree::make(quid->get_kind()));
      state_tree->graft(".quiddities." + quid_id, creation_information_tree);
    }

    // user data
    auto quid_user_data_tree = quid->user_data<MPtr(&InfoTree::get_tree)>(".");
    if (!quid_user_data_tree->empty()) {
      state_tree->graft(".userdata." + quid_id, quid_user_data_tree);
    }

    // writable property values
    quid->prop<MPtr(&quiddity::property::PBag::update_values_in_tree)>();
    auto props = quid->tree<MPtr(&InfoTree::get_child_keys)>("property");
    for (auto& prop : props) {
      // Don't save unwritable properties.
      if (!quid->tree<MPtr(&InfoTree::branch_get_value)>(std::string("property.") + prop +
                                                         ".writable"))
        continue;
      // Don't save properties with saving disabled.
      if (!quid->prop_is_saved(prop)) continue;
      state_tree->graft(".properties." + quid_id + "." + prop,
                        InfoTree::make(quid->tree<MPtr(&InfoTree::branch_get_value)>(
                            std::string("property.") + prop + ".value")));
    }
    // gets the state of all the connection
    auto connection_tree = get_connection_state();
    state_tree->graft(".connections", connection_tree);
  }
  // calling on_saved callback
  for (auto& quid_id : quid_ids) {
    qcontainer_->get_qrox(quid_id).get()->on_saved();
  }

  return state_tree;
}

void Switcher::apply_gst_configuration() {
  auto configuration = conf_.get();
  for (const auto& plugin : configuration->get_child_keys("gstreamer.primary_priority")) {
    int priority = configuration->branch_get_value("gstreamer.primary_priority." + plugin);
    if (!gst::Initialized::set_plugin_as_primary(plugin, priority)) {
      sw_warning("Unable to find Gstreamer plugin '{}'. Check if plugin is installed.", plugin);
    }
  }
}

bool Switcher::load_bundle_from_config(const std::string& bundle_description) {
  sw_debug("Receiving new bundles configuration: {}", bundle_description);
  auto bundles = infotree::json::deserialize(bundle_description);
  if (!bundles) {
    sw_error("Invalid bundle configuration.");
    return false;
  }

  auto new_configuration = InfoTree::copy(conf_.get().get());
  bool bundles_added = false;
  for (const auto& bundle_name : bundles->get_child_keys("bundle")) {
    if (new_configuration.get()->branch_get_copy(".bundle." + bundle_name) !=
        InfoTree::make_null()) {
      sw_warning("Bundle '{}' already exists. Skipping.", bundle_name);
      continue;
    }
    bundles_added = true;
    new_configuration->graft(".bundle." + bundle_name + ".",
                             bundles->branch_get_copy("bundle." + bundle_name));
  }

  if (bundles_added) {
    conf_.from_tree(new_configuration.get());
    register_bundle_from_configuration();
    sw_debug("New bundles added");
  }
  return true;
}

bool Switcher::register_bundle(InfoTree::ptr bundle_tree) {
  // retrieve available kinds vector
  auto kinds = qfactory_.get_kinds();
  // retrieve pointer to switcher configuration
  auto config = conf_.get();
  // retrieve bundle name from tree
  auto name = bundle_tree->get_child_keys(".").front();

  // register bundle in switcher configuration
  if (!config->graft("bundle", bundle_tree)) {
    sw_error("cannot register bundle `{}` into configuration", name);
    return false;
  }

  return true;
};

bool Switcher::unregister_bundle(InfoTree::ptr bundle_tree) {
  // retrieve available kinds vector
  auto kinds = qfactory_.get_kinds();
  // retrieve pointer to switcher configuration
  auto config = conf_.get();
  // retrieve bundle name from tree
  auto name = bundle_tree->get_child_keys(".").front();

  // register bundle in switcher configuration
  if (!config->prune("bundle." + name)) {
    sw_error("cannot prune bundle `{}` from configuration", name);
    return false;
  }

  return true;
};

void Switcher::register_bundle_from_configuration() {
  // registering bundle(s) as creatable kind
  auto quid_kinds = qfactory_.get_kinds();
  auto configuration = conf_.get();

  sw_debug("registering configuration: {}", conf_.get()->json());

  for (auto& it : configuration->get_child_keys("bundle")) {
    if (std::string::npos != it.find('_')) {
      sw_warning("underscores are not allowed for quiddity kinds (bundle name {})", it);
      continue;
    }
    std::string long_name = configuration->branch_get_value("bundle." + it + ".doc.long_name");
    std::string description = configuration->branch_get_value("bundle." + it + ".doc.description");
    std::string pipeline = configuration->branch_get_value("bundle." + it + ".pipeline");
    std::string is_missing;
    if (long_name.empty()) is_missing = "long_name";
    if (description.empty()) is_missing = "description";
    if (pipeline.empty()) is_missing = "pipeline";
    if (!is_missing.empty()) {
      sw_warning("{} : {} field is missing, cannot create new quiddity kind", it, is_missing);
      continue;
    }

    // check if the pipeline description is correct
    auto spec = quiddity::bundle::DescriptionParser(pipeline, quid_kinds);

    if (!spec) {
      sw_warning("{} : error parsing the pipeline ({})", it, spec.get_parsing_error());
      continue;
    }

    // ok, bundle can be added
    // Bundle names must be unique
    if (!quiddity::DocumentationRegistry::get()->register_doc(
            it, quiddity::Doc(long_name, it, description, "n/a", "n/a"))) {
      sw_debug("bundle '{}' already exists. Skipping.", it);
      continue;
    }

    qfactory_.register_kind_with_custom_factory(
        it, &quiddity::bundle::create, &quiddity::bundle::destroy);
    // making the new bundle kind available for next bundle definition:
    quid_kinds.push_back(it);
  }
}

void Switcher::remove_shm_zombies() const {
  auto files =
      fileutils::get_shmfiles_from_directory(this->get_shm_dir(), this->get_shm_prefix() + name_);
  for (auto& it : files) {
    auto res = fileutils::remove(it);
    if (!res) sw_warning("fail removing shmdata {} ({})", it, res.msg());
  }
}

void Switcher::scan_dir_for_plugins(const std::string& path) {
  if (fs::is_directory(path)) {
    qfactory_.scan_dir(path);
    // scanning sub-directories
    for (const auto& dir_entry : fs::recursive_directory_iterator(path)) {
      if (dir_entry.is_directory()) qfactory_.scan_dir(dir_entry.path());
    }
  } else {
    sw_warning("Switcher cannot scan {} when loading Quiddity plugins (not a directory)", path);
  }
}

}  // namespace switcher
