INSTALL
=======

> **Note**: Ensure **[Shmdata](https://gitlab.com/sat-mtl/tools/shmdata)** is already installed before proceeding.

## Quick build and installation (Ubuntu 22.04)

Build and install **switcher** from the command line:

```bash
# clone repository
sudo apt install git
git clone https://gitlab.com/sat-mtl/tools/switcher.git

cd switcher

# uncomment to checkout a specific version
# git checkout 3.5.4

# install pipx package manager
sudo apt install python3-pip
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# install dependencies
sudo apt install $(cat ./deps/apt-{build,runtime}-ubuntu-22.04)

# update or initialize git submodules recursively
git submodule update --init --recursive

# generate ninja recipes
cmake \
    -GNinja \
    -B build \
    -DCMAKE_BUILD_TYPE=Release \
    -DPLUGIN_VRPN=OFF \
    -DPLUGIN_WEBRTC=OFF \
    -DENABLE_GPL=ON

# compile and install libswitcher and pyquid on the system
ninja -C build
sudo ninja install -C build
# update shared library cache
sudo ldconfig
# test switcher command. If you get "switcher: command not found" you can try to install pipx as sudo
# and redo the ninja install step. This is not super clean but will get you there.
switcher --help
```


## Custom compilation

You can verify and change the build configuration using **ccmake**. To do so, you must first install the needed package:

```
$ sudo apt install cmake-curses-gui
```

Then, after running `cmake -B build`, from inside the build directory use:

```
$ ccmake ..
```

It will display a list of the configuration variables for the build.

When running non-interactive cmake you have to set the ENABLE\_GPL option if you want SIP and video features, otherwise they will be disabled by default:
```
$ cmake -B build -DENABLE_GPL=ON
```


## Other build options

* To run the tests:

```
    $ make test
```

* To generate debian installation packages (as configured in `CMakeLists.txt`):

```
    $ sudo apt install file
    $ make package # <- generate a debian package
```

* To generate a source package:

```
    $ make package_source
```

* To test the source package, this will create the source package and then try to build and test it

```
    $ make package_source_test
```
