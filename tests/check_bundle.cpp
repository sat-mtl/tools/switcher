/*
 * This file is part of switcher.
 *
 * switcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * switcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG  // get assert in release mode
#include <gst/gst.h>

#include <atomic>
#include <cassert>
#include <condition_variable>
#include <vector>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

using namespace switcher;

static bool success = false;
static std::atomic<bool> do_continue{true};
static std::condition_variable cond_var{};
static std::mutex mut{};

void wait_until_success() {
  // wait 3 seconds
  unsigned int count = 30;
  while (do_continue) {
    std::unique_lock<std::mutex> lock(mut);
    if (count == 0) {
      do_continue = false;
    } else {
      --count;
      cond_var.wait_for(lock, std::chrono::seconds(1), []() { return !do_continue.load(); });
    }
  }
}

// stops the wait in the wait_until_success function
void notify_success() {
  std::unique_lock<std::mutex> lock(mut);
  success = true;
  do_continue = false;
  cond_var.notify_one();
}

/**
 * Instanciates a dummy sink bundle and some sources (of kinds specified in the sources string
 * vector) and connects them with the specified connect_method using the writer connection spec
 * specified by the swid_label.
 *
 * \param manager Switcher instance used for the test
 * \param sources kind of the source bundles to connect
 * \param swid_label swid_label of the writer bundles
 * \param connect_method method to call to connect the bundles.
 * \param destination_quid kind of the destionation quid, instanciate a dummy-sink-bundle by
 * default. \return true on test success, false otherwise
 */
bool test_bundle_connections(Switcher::ptr manager,
                             std::vector<std::string> sources,
                             std::string swid_label,
                             std::string connect_method,
                             std::string destination_quid = "dummy-sink-bundle") {
  // sets the test variables
  do_continue = true;
  success = false;

  // instanciate the source bundles
  auto dummyqrox = manager->quiddities->create(destination_quid, "dummy", nullptr);
  assert(dummyqrox);
  auto dummy = dummyqrox.get();

  std::vector<quiddity::qid_t> source_ids;

  for (const auto& current_source : sources) {
    // instanciate a source with a unique id.
    auto srcqrox = manager->quiddities->create(
        current_source, "src" + std::to_string(source_ids.size()), nullptr);
    assert(srcqrox);
    // keeps the source id for cleanup afterwards.
    source_ids.push_back(srcqrox.get_id());
    // starts the source
    assert(srcqrox.get()->prop<MPtr(&quiddity::property::PBag::set_str_str)>("started", "true"));

    // find the swid of the connection spec with the right label for the source.
    auto swid = srcqrox.get()->claw<MPtr(&quiddity::claw::Claw::get_swid)>(swid_label);
    // find the shmdata path associated with this swid
    std::string writer_shmpath =
        srcqrox.get()->claw<MPtr(&quiddity::claw::Claw::get_writer_shmpath)>(swid);
    manager->sw_debug("writer shmpath : {}\n", writer_shmpath);

    // subscribe to the on-tree-grafted callback of the sink bundle so we can stop waiting for the
    // connection to happen once it happens.
    quiddity::signal::register_id_t callback_sub_id =
        dummy->sig<MPtr(&quiddity::signal::SBag::subscribe_by_name)>(
            "on-tree-grafted", [&](const InfoTree::ptr& tree) {
              if (Any::to_string(tree->branch_get_value(".")) ==
                  ".shmdata.reader." + writer_shmpath) {
                notify_success();
              }
            });

    // gets the sfid of the sink
    auto sfid = dummy->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("dummy/default");

    // stores the connection_sfid to disconnect at the end.
    quiddity::claw::sfid_t connection_sfid = Ids::kInvalid;

    // either connects a specific swid to a specific sfid (connect) or connect the first
    // compatible swid to the first compatible sfid (try_connect)
    if (connect_method == "connect") {
      connection_sfid =
          dummy->claw<MPtr(&quiddity::claw::Claw::connect)>(sfid, srcqrox.get_id(), swid);
      assert(connection_sfid);
    } else if (connect_method == "try_connect") {
      connection_sfid = dummy->claw<MPtr(&quiddity::claw::Claw::try_connect)>(srcqrox.get_id());
      assert(connection_sfid);
    }
    // wait until the connection is confirmed in the on-tree-gradted method
    wait_until_success();
    // get the grafted sfid from the shmdata part of the dummy sink infotree.
    quiddity::claw::sfid_t sfid_from_shmdata_tree =
        dummy->tree<MPtr(&InfoTree::get_copy)>()->branch_get_value(".shmdata.reader." +
                                                                   writer_shmpath + ".sfid");
    // gets the sfid stored in the follower_shmpath_ map in the dummy's claw.
    auto sfid_from_reader_claw =
        dummy->claw<MPtr(&quiddity::claw::Claw::get_follower_sfid)>(writer_shmpath);

    manager->sw_debug("sfid_from_reader_claw {}\n", sfid_from_reader_claw);
    manager->sw_debug("sfid_from_shmdata_tree {}\n", sfid_from_shmdata_tree);

    // they should be the same.
    assert(sfid_from_shmdata_tree == sfid_from_reader_claw);

    // stops the source quiddity
    assert(srcqrox.get()->prop<MPtr(&quiddity::property::PBag::set_str_str)>("started", "false"));

    // this will be called after 30 second if a connection did not happen.
    if (!success) {
      manager->sw_error("No data received.");
      return false;
    }

    // Reset the test variables.
    do_continue = true;
    success = false;

    // subscribe to on-tree-pruned to be notified of disconnection
    quiddity::signal::register_id_t pruned_callback_sub_id =
        dummy->sig<MPtr(&quiddity::signal::SBag::subscribe_by_name)>(
            "on-tree-pruned", [&](const InfoTree::ptr& tree) {
              if (Any::to_string(tree->branch_get_value(".")) ==
                  ".shmdata.reader." + writer_shmpath) {
                notify_success();
              }
            });
    // disconnect the source
    assert(dummy->claw<MPtr(&quiddity::claw::Claw::disconnect)>(connection_sfid));
    // wait until the confirmation of the disconnection
    wait_until_success();
    // unsubscribe from the callbacks
    dummy->sig<MPtr(&quiddity::signal::SBag::unsubscribe_by_name)>("on-tree-pruned",
                                                                   pruned_callback_sub_id);
    dummy->sig<MPtr(&quiddity::signal::SBag::unsubscribe_by_name)>("on-tree-grafted",
                                                                   callback_sub_id);
    // reset the test variables.
    do_continue = true;
    success = false;
  }

  // cleanup quiddities
  manager->quids<MPtr(&quiddity::Container::remove)>(dummyqrox.get_id());
  for (const auto& source_id : source_ids) {
    manager->quids<MPtr(&quiddity::Container::remove)>(source_id);
  }

  return true;
}

/**
 * This function is very similar to test_bundle_connections but it tests wether or not
 * the shmdata follower of a bundle receives a frame.
 * \param manager Instance of switcher to use for quiddity initialization
 * \return true if it succeeds, false if it doesnt
 */
bool test_bundle_shmdata_communication(Switcher::ptr manager) {
  // testing shmdata communication from a bundle to an other
  auto srcqrox = manager->quiddities->create("vid-source-bundle", "src", nullptr);
  assert(srcqrox);
  auto dummyqrox = manager->quiddities->create("dummy-sink-bundle", "dummy", nullptr);
  assert(dummyqrox);
  auto dummy = dummyqrox.get();

  assert(srcqrox.get()->prop<MPtr(&quiddity::property::PBag::set_str_str)>("started", "true"));
  assert(
      0 !=
      dummy->prop<MPtr(&quiddity::property::PBag::subscribe)>(
          dummy->prop<MPtr(&quiddity::property::PBag::get_id)>("dummy/frame-received"), [&]() {
            if (dummy->prop<MPtr(&quiddity::property::PBag::get<bool>)>(
                    dummy->prop<MPtr(&quiddity::property::PBag::get_id)>("dummy/frame-received"))) {
              notify_success();
            }
          }));
  auto sfid = dummy->claw<MPtr(&quiddity::claw::Claw::get_sfid)>("dummy/default");
  auto swid = srcqrox.get()->claw<MPtr(&quiddity::claw::Claw::get_swid)>("encoder/video-encoded");
  assert(dummy->claw<MPtr(&quiddity::claw::Claw::connect)>(sfid, srcqrox.get_id(), swid));

  wait_until_success();
  assert(srcqrox.get()->prop<MPtr(&quiddity::property::PBag::set_str_str)>("started", "false"));
  if (!success) {
    manager->sw_error("No data received.");
    return false;
  }
  manager->quids<MPtr(&quiddity::Container::remove)>(dummyqrox.get_id());
  manager->quids<MPtr(&quiddity::Container::remove)>(srcqrox.get_id());
  // resets the test variables
  do_continue = true;
  success = false;
  return true;
}

int main() {
  // making the switcher
  Switcher::ptr manager = Switcher::make_switcher("check_bundle_test", true);

  // loading configuration with bundle
  assert(manager->conf<MPtr(&Configuration::from_file)>("./check_bundle.config"));

  // creating and removing some complex bundles
  auto bundles = {
      "video-test-input", "source-bundle", "sink-bundle", "filter-bundle", "whitespaces-bundle"};
  for (const auto& bundle : bundles) {
    assert(quiddity::test::create(manager, bundle));
  }

  // test different connection modalities of bundle quiddities.
  std::vector<std::string> sources_test_1 = {"vid-source-bundle", "vid-source-bundle"};

  manager->sw_debug("two source bundles with two writers and connect method on swid\n");
  assert(test_bundle_connections(manager, sources_test_1, "encoder/video-encoded", "connect"));

  manager->sw_debug(
      "two source bundles with two writers and try_connect method (will take the first compatible "
      "swid which always has the source/video tag\n");
  assert(test_bundle_connections(manager, sources_test_1, "source/video", "try_connect"));

  manager->sw_debug(
      "two source bundles with two writers with try_connect on a bundle that accepts encoded video "
      "only.\n");
  assert(test_bundle_connections(
      manager, sources_test_1, "encoder/video-encoded", "try_connect", "rtmpOutput"));

  std::vector<std::string> sources_test_2 = {"video-test-input", "video-test-input"};

  manager->sw_debug("two source bundles with one writer and connect method on\n");
  assert(test_bundle_connections(manager, sources_test_2, "Generator/video", "connect"));

  manager->sw_debug("two source bundles with two writers and try_connect method on swid\n");
  assert(test_bundle_connections(manager, sources_test_2, "Generator/video", "try_connect"));

  // test transmission of shmdata frames.
  assert(test_bundle_shmdata_communication(manager));

  gst_deinit();
  return 0;
}
