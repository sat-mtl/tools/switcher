/*
 * This file is part of switcher.
 *
 * switcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * switcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switcher.  If not, see <http://www.gnu.org/licenses/>.
 */

/* To run test manually, make sure you change your current working directory
 * to build/tests directory, then invoke check_global_configuration test
 * executable like so.
 *   $ cd build/tests
 *   $ ./check_global_configuration
 */

#undef NDEBUG  // get assert in release mode

#include <cassert>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

using namespace switcher;
namespace fs = std::filesystem;

int main() {
  // get `global configuration file` path
  const fs::path global_config_file_path = Configuration::get_default_global_path();

  // 1. rename existing global.json to global.json.check_global_configuration_bak
  fs::path global_config_backup_file_path = global_config_file_path;
  const std::string backup_extension = ".json.check_global_configuration_bak_";
  global_config_backup_file_path.replace_extension(".json.check_global_configuration_bak");
  if (fs::exists(global_config_file_path))
    fs::copy_file(
        global_config_file_path, global_config_backup_file_path, fs::copy_options::skip_existing);

  // 2. install example global configuration file from current working directory
  // to /etc/switcher/global.json or ~/.config/switcher/global.json
  fs::create_directories(global_config_file_path.parent_path());
  assert(fs::copy_file("./check_global_configuration.json",
                       global_config_file_path,
                       fs::copy_options::overwrite_existing));

  // 3. switcher load example global configuration from file
  Switcher::ptr mgr = Switcher::make_switcher("test-global-config", true);

  // 4. check that testKey is present
  assert(mgr->conf<MPtr(&Configuration::get_value)>(".testKey").not_null());

  // 5. check that testKey value is testValue
  std::string value = mgr->conf<MPtr(&Configuration::get_value)>(".testKey");
  assert(0 == value.compare("testValue"));

  // 6. restore backup global configuration if present
  if (fs::exists(global_config_backup_file_path)) {
    fs::copy_file(global_config_backup_file_path,
                  global_config_file_path,
                  fs::copy_options::overwrite_existing);
    fs::remove(global_config_backup_file_path);
  }

  return 0;
}
