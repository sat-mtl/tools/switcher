/*
 * This file is part of switcher.
 *
 * libswitcher is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#undef NDEBUG  // get assert in release mode

#include <unistd.h>

#include <atomic>
#include <cassert>
#include <condition_variable>

#include "switcher/infotree/information-tree.hpp"
#include "switcher/infotree/json-serializer.hpp"
#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

static bool success = false;
static std::atomic<bool> do_continue{true};
static std::condition_variable cond_var{};
static std::mutex mut{};

void wait_until_success() {
  // wait 3 seconds
  unsigned int count = 30;
  while (do_continue) {
    std::unique_lock<std::mutex> lock(mut);
    if (count == 0) {
      do_continue = false;
    } else {
      --count;
      cond_var.wait_for(lock, std::chrono::seconds(1), []() { return !do_continue.load(); });
    }
  }
}

void notify_success() {
  std::unique_lock<std::mutex> lock(mut);
  success = true;
  do_continue = false;
  cond_var.notify_one();
}

int main() {
  using namespace switcher;
  using namespace switcher::quiddity::property;
  using namespace switcher::quiddity::claw;

  Switcher::ptr swit = Switcher::make_switcher("test_manager");

  // gets a dummysink quid that accepts anything.
  auto reader = swit->quiddities->create("dummysink", "reader", nullptr).get();
  assert(reader);
  // and a videotestsrc to be sure we have some actual data passing through
  auto writer = swit->quiddities->create("videotestsrc", "writer", nullptr).get();
  assert(writer);
  // starts the video.
  writer->prop<MPtr(&PBag::set_str_str)>("started", "true");

  // this registers a callback to the frame-received property of the dummysink.
  // this will call notify_success once the sink receives a frame.
  assert(0 != reader->prop<MPtr(&PBag::subscribe)>(
                  reader->prop<MPtr(&PBag::get_id)>("frame-received"), [&]() {
                    if (reader->prop<MPtr(&PBag::get<bool>)>(
                            reader->prop<MPtr(&PBag::get_id)>("frame-received"))) {
                      notify_success();
                    }
                  }));
  // connects the quiddities
  auto res_id = reader->claw<MPtr(&Claw::try_connect)>(writer->get_id());
  // We need to wait here because the sfid of the Follower shmdata is not set
  // until the on_server_connected callback is called and it is only called when
  // some data is received.
  wait_until_success();

  // gets the swid associated with the video label and then gets the shmpath
  // associated with the swid
  auto writer_swid = writer->claw<MPtr(&Claw::get_swid)>("video");
  auto writer_shmpath = writer->claw<MPtr(&Claw::get_writer_shmpath)>(writer_swid);

  // checks to see if the sfid property exists in the infotree of the follower
  // shmdata
  sfid_t sfid_from_shmdata_tree = reader->tree<MPtr(&InfoTree::get_copy)>()->branch_get_value(
      ".shmdata.reader." + writer_shmpath + ".sfid");
  auto sfid_from_reader_claw = reader->claw<MPtr(&Claw::get_follower_sfid)>(writer_shmpath);
  assert(sfid_from_shmdata_tree == sfid_from_reader_claw);

  // set the connect-called property to false and calls try_connect with the same source.
  // The claw instance should see that the same shmdata is going to be connected and should not
  // call the on_connect_cb_ of the quiddity, which will result in connect not being called
  // for the dummysink. We verify that by sleeping a second and verifying that the property is still
  // false.
  reader->prop<MPtr(&PBag::set_str_str)>("connect-called", "false");
  res_id = reader->claw<MPtr(&Claw::try_connect)>(writer->get_id());
  assert(reader->prop<MPtr(&PBag::get_str_str)>("connect-called") == "false");

  // verify that our sink was never disconnected
  assert(reader->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "false");

  // we create another video quiddity and call try_connect with the sink.
  // the claw instance should realize that the only sfid of the sink is already connected to
  // a shmdata and that it should disconnect whatever is there before connecting anything.
  auto writer_2 = swit->quiddities->create("videotestsrc", "writer2", nullptr).get();

  // we can verify all that by checking that the connect-called and disconnect-called properties
  // were set to true.
  res_id = reader->claw<MPtr(&Claw::try_connect)>(writer_2->get_id());

  assert(reader->prop<MPtr(&PBag::get_str_str)>("connect-called") == "true");
  assert(reader->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "true");

  // and then disconnect
  assert(reader->claw<MPtr(&Claw::disconnect)>(res_id));

  // This next section checks that on removal of a quiddity, all follower connected to its
  // writers are disconnected.
  // gets a bunch of dummysinks
  auto reader0 = swit->quiddities->create("dummysink", "reader0", nullptr).get();
  auto reader1 = swit->quiddities->create("dummysink", "reader1", nullptr).get();
  auto reader2 = swit->quiddities->create("dummysink", "reader2", nullptr).get();
  // make a writer that we will remove.
  auto removed_writer = swit->quiddities->create("videotestsrc", "removed_writer", nullptr).get();
  assert(removed_writer);
  // starts the video.
  removed_writer->prop<MPtr(&PBag::set_str_str)>("started", "true");

  // verify that disconnect was never called.
  assert(reader0->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "false");
  assert(reader1->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "false");
  assert(reader2->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "false");
  // connects the same writer to all of them.
  reader0->claw<MPtr(&Claw::try_connect)>(removed_writer->get_id());
  reader1->claw<MPtr(&Claw::try_connect)>(removed_writer->get_id());
  reader2->claw<MPtr(&Claw::try_connect)>(removed_writer->get_id());
  // removes the writer.
  swit->quids<MPtr(&quiddity::Container::remove)>(removed_writer->get_id());
  // All the disconnects should have been called.
  assert(reader0->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "true");
  assert(reader1->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "true");
  assert(reader2->prop<MPtr(&PBag::get_str_str)>("disconnect-called") == "true");

  // this next bit connects the writer to reader0, verifies that the connection was
  // registered in the claw of the writer, removes reader0 and verifies that the connection
  // was unregistered. This proves that the life cycle of writer side connections in claw is
  // properly managed

  // checks that the connection size is 0.
  auto connection_size =
      writer->claw<MPtr(&quiddity::claw::Claw::get_registered_writer_connections)>(writer_swid)
          .size();
  assert(connection_size == 0);

  reader0->claw<MPtr(&Claw::try_connect)>(writer->get_id());
  // checks that the connection was registered
  connection_size =
      writer->claw<MPtr(&quiddity::claw::Claw::get_registered_writer_connections)>(writer_swid)
          .size();
  assert(connection_size == 1);
  // does some connections and disconnections to see if they are properly added
  // and removed
  auto reader_1_sfid = reader1->claw<MPtr(&Claw::try_connect)>(writer->get_id());
  auto reader_2_sfid = reader2->claw<MPtr(&Claw::try_connect)>(writer->get_id());
  connection_size =
      writer->claw<MPtr(&quiddity::claw::Claw::get_registered_writer_connections)>(writer_swid)
          .size();
  assert(connection_size == 3);
  reader1->claw<MPtr(&Claw::disconnect)>(reader_1_sfid);
  reader2->claw<MPtr(&Claw::disconnect)>(reader_2_sfid);
  connection_size =
      writer->claw<MPtr(&quiddity::claw::Claw::get_registered_writer_connections)>(writer_swid)
          .size();
  assert(connection_size == 1);
  // removes the reader0.
  swit->quids<MPtr(&quiddity::Container::remove)>(reader0->get_id());
  // checks that the size of the connection is back to 0, which means that the connection was
  // unregistered.
  connection_size =
      writer->claw<MPtr(&quiddity::claw::Claw::get_registered_writer_connections)>(writer_swid)
          .size();
  assert(connection_size == 0);

  return 0;
}
