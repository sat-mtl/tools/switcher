#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

import time

from .base import SocketIOTestCase

# test quiddity data
videotest_quid = {"kind": "videotestsrc", "nickname": "test"}

signal_quid = {"kind": "signal", "nickname": "signal"}


class PropertyTestCase(SocketIOTestCase):
    """Test case for the `UserTree API`"""

    # reverences on the created quiddities
    videotest_id = None

    def setUp(self):
        # creation of a videotestsrc quiddity
        data = tuple(videotest_quid.values())
        err, res = self.sio.call(
            "quiddity_create", data=data, namespace=self.namespace
        )
        self.assertIsNone(err)
        self.videotest_id = res["id"]

    def tearDown(self):
        err, res = self.sio.call("session_clear", namespace=self.namespace)
        self.videotest_id = None
        self.signal_id = None
        self.rcvd_data = []

    events = ["property_updated"]

    def test_set_property(self):
        err, res = self.sio.call(
            "property_set",
            data=(self.videotest_id, "resolution", "2"),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, str)
        self.assertEqual(res, "2")

    def test_get_property(self):
        err, res = self.sio.call(
            "property_get",
            data=(self.videotest_id, "resolution"),
            namespace=self.namespace,
        )
        self.assertIsNone(err)
        self.assertIsInstance(res, str)
        self.assertEqual(res, "1")

    def test_property_updated(self):
        err, res = self.sio.call(
            "property_set",
            data=(self.videotest_id, "resolution", "2"),
            namespace=self.namespace,
        )
        time.sleep(0.1)
        property_event, property_grafted_data = self.rcvd_data.pop()
        self.assertEqual(property_event, "property_updated")
        quid_id, propName, value, propId = property_grafted_data
        self.assertEqual(quid_id, self.videotest_id)
        self.assertEqual(propName, "resolution")
        self.assertEqual(value, "2")
