import asyncio
import sys
from importlib.metadata import version as importlib_metadata_version

import socketio
from aiohttp import web

from .switcherio_server_namespace import SwitcherIOServerNamespace

SWITCHERIO_NAMESPACE = "/switcherio"


class SwitcherIOServer:
    _port = None
    _room = None
    _aiohttp_app = None
    _socketio_server = None
    _switcherio_server_namespace = None

    def __init__(
        self,
        debug_switcher=False,
        debug_socketio=False,
        port=8000,
        room="standalone",
    ):
        self._port = port
        self._room = room
        self._switcherio_server_namespace = SwitcherIOServerNamespace(
            SWITCHERIO_NAMESPACE, room, debug_switcher
        )

        self._aiohttp_app = web.Application()
        self._aiohttp_app.on_shutdown.append(self.on_shutdown)
        self._aiohttp_app.on_cleanup.append(self.on_cleanup)

        self._socketio_server = socketio.AsyncServer(
            logger=debug_socketio,
            async_mode="aiohttp",
            cors_allowed_origins="*",
            ping_interval=1,
            ping_timeout=20,
        )
        self._socketio_server.register_namespace(
            self._switcherio_server_namespace
        )
        self._socketio_server.attach(self._aiohttp_app)

        python_version = sys.version
        self._socketio_server.logger.info(
            f"SwitcherIOServer::__init__: Python version {python_version}"
        )
        switcherio_version = importlib_metadata_version("switcherio")
        self._socketio_server.logger.info(
            f"SwitcherIOServer::__init__: switcherio version {switcherio_version}"
        )
        aiohttp_version = importlib_metadata_version("aiohttp")
        self._socketio_server.logger.info(
            f"SwitcherIOServer::__init__: aiohttp version {aiohttp_version}"
        )
        python_socketio_version = importlib_metadata_version("python-socketio")
        self._socketio_server.logger.info(
            f"SwitcherIOServer::__init__: python-socketio version {python_socketio_version}"
        )
        self._socketio_server.logger.info(
            "SwitcherIOServer::__init__: initialized."
        )

    def switcher_version(self):
        return self._switcherio_server_namespace.switcher_version()

    async def on_shutdown(self, app):
        self._socketio_server.logger.info("Caught interrupt signal.")

        self._socketio_server.logger.info("Disconnecting clients...")
        try:
            for participant in self._socketio_server.manager.get_participants(
                namespace=SWITCHERIO_NAMESPACE, room=self._room
            ):
                self._socketio_server.logger.info(
                    f"Disconnecting client {participant}"
                )
                await self._socketio_server.leave_room(
                    participant, self._room, SWITCHERIO_NAMESPACE
                )
                await self._socketio_server.disconnect(participant)
        except KeyError:
            pass

        await self._socketio_server.close_room(
            self._room, namespace=SWITCHERIO_NAMESPACE
        )

        await self._socketio_server.shutdown()

        self._switcherio_server_namespace.cleanup()

        self._socketio_server.logger.info("SwitcherIOServer shutdown done.")

    async def on_cleanup(self, app):
        print("Closing SwitcherIO server...")

        # GracefulExit signal is private to aiohttp.web, but we need to
        # call it to release http socket. This is needed so we can re-run
        # server multiple times during unit tests
        # ref.: https://github.com/aio-libs/aiohttp/issues/2950
        raise web.GracefulExit()

    def run(self):
        self._socketio_server.logger.info("Running SwitcherIO server...")
        web.run_app(
            self._aiohttp_app, port=self._port, loop=asyncio.get_event_loop()
        )
        print("Finished running SwitcherIO server.")
