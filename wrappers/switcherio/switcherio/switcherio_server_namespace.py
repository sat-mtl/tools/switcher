import asyncio
import json
import sys
from importlib.metadata import version as importlib_metadata_version
from typing import Any, Dict, List, Optional, Tuple, Union

import socketio
from pyquid import InfoTree, Quiddity, Switcher


class SwitcherIOServerNamespace(socketio.AsyncNamespace):
    _asyncio_event_loop = asyncio.get_event_loop()
    _namespace = str
    _room = str
    _sw: Optional[Switcher] = None
    _quiddity_cache = {}

    def __init__(self, namespace, room, debug):
        super().__init__(namespace)
        self._namespace = namespace
        self._room = room
        if self._sw is None:
            self._set_switcher_instance(
                Switcher(name="switcher-ws", debug=debug)
            )

        python_version = sys.version
        self._log(
            "debug",
            f"SwitcherIOServerNamespace::__init__: Python version {python_version}",
        )
        python_socketio_version = importlib_metadata_version("switcherio")
        self._log(
            "debug",
            f"SwitcherIOServerNamespace::__init__: switcherio version {python_socketio_version}",
        )
        python_socketio_version = importlib_metadata_version("python-socketio")
        self._log(
            "debug",
            f"SwitcherIOServerNamespace::__init__: python-socketio version {python_socketio_version}",
        )
        self._log(
            "debug",
            f'SwitcherIOServerNamespace::__init__: Mapping API under Socket.IO namespace "{self._namespace}"',
        )
        self._log(
            "debug",
            f'SwitcherIOServerNamespace::__init__: Initial Socket.IO room to join "{self._room}"',
        )
        self._log("debug", "SwitcherIOServerNamespace::__init__: initialized.")

    def cleanup(self):
        self.server.logger.info("SwitcherIOServerNamespace cleanup...")
        self.server.logger.info("Stopping Switcher...")
        self._sw.stop()
        self.server.logger.info("Switcher stopped.")

    def switcher_version(self):
        return self._sw.version()

    # SwitcherIO utilities
    # ==================

    def _log(
        self, log_level: str, msg: str, sid: str = ""
    ) -> Tuple[Optional[str], bool]:
        """Logging helper that forwards swio logging messages to Switcher logger

        Arguments:
            log_level {str} -- Log level of message.
            msg {str} -- Text message to forward to Switcher logger.
            sid {str} -- Socket.io client session ID from originating event.
                        sid can be ommited if log message originates from swio
                        server itself.

        Returns:
            tuple -- Exception and response from Switcher logger
        """
        msg = f"/{sid} {msg}"
        try:
            logger_method = getattr(self._sw.logger, log_level)
            logger_method(msg)
            return None, True
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def _get_quiddity_model(self, quid: Quiddity) -> dict:
        """Get the full model of a quiddity

        Arguments:
            quid {pyquid.Quiddity} -- A quiddity object from pyquid

        Returns:
            dict -- The quiddity model as a dictionnary
        """
        model = json.loads(str(quid))

        model["nickname"] = quid.nickname()
        model["infoTree"] = json.loads(str(quid.get_info_tree_as_json()))
        model["connectionSpecs"] = json.loads(str(quid.get_connection_specs()))
        model["userTree"] = json.loads(str(quid.get_user_tree()))

        # the json model should contain empty trees instead of null values
        if model["userTree"] is None:
            model["userTree"] = {}

        if model["connectionSpecs"] is None:
            model["connectionSpecs"] = {}

        return model

    def _set_switcher_instance(self, instance: Switcher) -> None:
        if self._sw is not None:
            self._sw.unsubscribe("on-quiddity-created")
            self._sw.unsubscribe("on-quiddity-removed")
        # resets the quiddity cache
        self._quiddity_cache = {}
        self._sw = instance

        # Properties subscription for existing quiddities
        for quid in self._sw.quiddities:
            # sets the cache for that id.
            self._quiddity_cache[quid.id()] = quid
            self._set_property_subscriptions(quid)

        # Switcher signals subscription
        self._sw.subscribe("on-quiddity-created", self._on_quiddity_created)
        self._sw.subscribe("on-quiddity-removed", self._on_quiddity_deleted)

    # Switcher Callbacks
    # ==================

    def _on_quiddity_created(self, quid_id: str) -> None:
        """Switcher callback to notify clients of successful quiddity creation.

        This is called by Switcher whenever a quiddity is created and emits
        a `quiddity_created` event to all clients in the room.

        Arguments:
            quid_id {str} -- The identifier of the newly created `Quiddity`.
        """

        try:
            quid = self._get_quiddity_from_id(quid_id)
            self._set_signal_subscriptions(quid)
            self._set_property_subscriptions(quid)

            self._log("debug", f"quiddity_created(quid_id: {quid_id})")
            asyncio.run_coroutine_threadsafe(
                self.emit("quiddity_created", self._get_quiddity_model(quid)),
                self._asyncio_event_loop,
            )
        except Exception as e:
            self.server.logger.exception(e)

    def _on_quiddity_deleted(self, quid_id: str) -> None:
        """Switcher callback to notify clients of successful quiddity removal.

        This is called by Switcher whenever a quiddity is removed and emits
        a `quiddity_deleted` event to all clients in the room.

        Arguments:
            quid_id {str} -- The identifier of the removed `Quiddity`.
        """
        self._log("debug", f"quiddity_deleted(quid_id: {quid_id})")
        asyncio.run_coroutine_threadsafe(
            self.emit("quiddity_deleted", quid_id), self._asyncio_event_loop
        )

    def _on_quiddity_updated(
        self, quid_id: str, updated: Optional[dict]
    ) -> None:
        """Switcher callback to notify clients about how went the quiddity update

        This is called by Switcher whenever a quiddity is updated and emits
        a `quiddity_updated` event to all clients in the room.

        Arguments:
            quid_id {str} -- The identifier of the removed `Quiddity`.
            updated {Optional[dict]} -- The dictionnary of updated properties
        """
        self._log(
            "debug",
            f"quiddity_updated(quid_id: {quid_id}, updated: {updated})",
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("quiddity_updated", quid_id, updated),
            self._asyncio_event_loop,
        )

    def _on_user_tree_grafted(self, tree_path: InfoTree, quid_id: str) -> None:
        """Switcher callback to notify clients that the user_tree is grafted

        Arguments:
            tree_path {InfoTree} -- An `InfoTree` that contains the grafted path.
            quid_id {str} -- The identifier of the updated `Quiddity`.
        """
        quid = self._get_quiddity_from_id(quid_id)
        path = tree_path.get()
        value = json.loads(quid.get_user_tree_as_json(path))
        self._log(
            "debug",
            f"user_tree_grafted(quid_id: {quid_id}, path: {path}), value: {value}",
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("user_tree_grafted", (quid_id, path, value)),
            self._asyncio_event_loop,
        )

    def _on_user_tree_pruned(self, tree_path: InfoTree, quid_id: str) -> None:
        """Switcher callback to notify clients that the user_tree is pruned

        Arguments:
            tree_path {InfoTree} -- An `InfoTree` that contains the pruned path.
            quid_id {str} -- The identifier of the updated `Quiddity`.
        """
        path = tree_path.get()
        self._log(
            "debug", f"user_tree_pruned(quid_id: {quid_id}, path: {path})"
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("user_tree_pruned", (quid_id, path)),
            self._asyncio_event_loop,
        )

    def _on_info_tree_grafted(self, tree_path: InfoTree, quid_id: str) -> None:
        """Switcher callback to notify clients that the info_tree is updated

        Arguments:
            tree_path {InfoTree} -- An `InfoTree` that contains the updated path.
            quid_id {str} -- The identifier of the updated `Quiddity`.
        """
        if quid_id not in self._sw.list_ids():
            return
        quid = self._get_quiddity_from_id(quid_id)
        path = tree_path.get()
        value = quid.get_info_tree_as_json(path)
        # If we are grafting a new property, we need to subscribe to it with a callback
        if len(path.split(".")) == 2 and path.startswith("property."):
            self._reset_property_subscriptions(quid_id)

        # mute systemusage and shmdata stat updates from logging
        is_systemusage = quid.get_kind() == "systemusage"
        is_stat = path.endswith(".stat")
        if not is_systemusage and not is_stat:
            self._log(
                "debug",
                f"info_tree_grafted(quid_id: {quid_id}, path: {path}, value: {value})",
            )

        asyncio.run_coroutine_threadsafe(
            self.emit("info_tree_grafted", (quid_id, path, value)),
            self._asyncio_event_loop,
        )

    def _on_info_tree_pruned(self, tree_path: InfoTree, quid_id: str) -> None:
        """Switcher callback to notify clients that the info_tree is pruned

        Arguments:
            tree_path {InfoTree} -- An `InfoTree` that contains the updated path.
            quid_id {str} -- The identifier of the updated `Quiddity`.
        """
        if quid_id not in self._sw.list_ids():
            return
        path = tree_path.get()

        # If we are pruning a new property, we need to remove the callbacks to it.
        if len(path.split(".")) == 2 and path.startswith("property."):
            self._reset_property_subscriptions(quid_id)

        self._log(
            "debug", f"info_tree_pruned(quid_id: {quid_id}, path: {path})"
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("info_tree_pruned", (quid_id, path)),
            self._asyncio_event_loop,
        )

    def _on_connection_spec_added(
        self, tree_path: InfoTree, quid_id: str
    ) -> None:
        """Switcher callback to notify clients that a connection spec is added.

        Arguments:
            tree_path {InfoTree} -- An `InfoTree` that contains the updated connection spec.
            quid_id {str} -- The identifier of the updated `Quiddity`.
        """
        if quid_id not in self._sw.list_ids():
            return

        quid = self._get_quiddity_from_id(quid_id)
        path = tree_path.get()
        value = json.loads(quid.get_connection_spec_tree_as_json(path))

        self._log(
            "debug",
            f"connection_spec_added(quid_id: {quid_id}, path: {path}, value: {value})",
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("connection_spec_added", (quid_id, path, value)),
            self._asyncio_event_loop,
        )

    def _on_connection_spec_removed(
        self, tree_path: InfoTree, quid_id: str
    ) -> None:
        """Switcher callback to notify clients that a connection spec is removed.

        Arguments:
            tree_path {InfoTree} -- An `InfoTree` that contains the updated connection spec.
            quid_id {str} -- The identifier of the updated `Quiddity`.
        """
        if quid_id not in self._sw.list_ids():
            return
        path = tree_path.get()

        self._log(
            "debug",
            f"connection_spec_removed(quid_id: {quid_id}, path: {path})",
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("connection_spec_removed", (quid_id, path)),
            self._asyncio_event_loop,
        )

    def _on_connected(self, connection, quid_id):
        """Switcher callback to notify clients that a connection to a quiddity was made.

        Arguments:
            connection {InfoTree} -- An `InfoTree` that contains the description of the new connection.
            quid_id {str} -- The identifier of the quiddity to which something was connected.
        """
        if quid_id not in self._sw.list_ids():
            return
        connection_json = connection.json()
        self._log(
            "debug",
            f"connected(quid_id: {quid_id}, connection: {connection_json})",
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("connected", (quid_id, connection_json)),
            self._asyncio_event_loop,
        )

    def _on_disconnected(self, connection, quid_id):
        """Switcher callback to notify clients that something was diconnected from a quiddity.
        Arguments:
            connection {InfoTree} -- An `InfoTree` that contains the description of the connection that is now gone.
            quid_id {str} -- The identifier of the quiddity to which something was connected.
        """
        if quid_id not in self._sw.list_ids():
            return
        connection_json = connection.json()
        self._log(
            "debug",
            f"disconnected(quid_id: {quid_id}, connection: {connection_json})",
        )
        asyncio.run_coroutine_threadsafe(
            self.emit("disconnected", (quid_id, connection_json)),
            self._asyncio_event_loop,
        )

    def _on_property_updated(
        self, value: Any, prop_data: Dict[str, str]
    ) -> None:
        """Switcher callback to notify clients of the successful property update.

        This is called by Switcher whenever a property is updated and emits
        a `property.updated` event to all clients in the room.

        Arguments:
            value {Any} -- The new value of the property
            prop_data {Dict[str, str]} -- A dictionnary that contains the name and
                                        identifier of the updated property along
                                        with the identifier of the quiddity it
                                        belongs to
        """
        try:
            quid_id, property_name, property_id = prop_data.values()

            # mute thumbnail and preview last_image property updates from logging
            is_last_image = property_name.endswith("last_image")
            if not is_last_image:
                self._log(
                    "debug",
                    f"property_updated(quid_id: {quid_id}, "
                    f"property_name: {property_name}, "
                    f"value: {value}, "
                    f"property_id: {property_id})",
                )

            asyncio.run_coroutine_threadsafe(
                self.emit(
                    "property_updated",
                    (quid_id, property_name, value, property_id),
                ),
                self._asyncio_event_loop,
            )
        except KeyError as e:
            self.server.logger.exception(e)

    def _on_nickname_updated(self, nickname: str, quid_id: str) -> None:
        """Switcher callback to notify clients of successful nickname update.

        This is called by Switcher whenever a nickname is updated and emits
        a `nickname.updated` event to all clients in the room.

        Arguments:
            nickname {str} -- The new nickname of the quiddity
            quid_id {str} -- The identifier of the updated quiddity
        """
        try:
            self._log(
                "debug",
                f"nickname_updated(quid_id: {quid_id}, nickname: {nickname})",
            )
            asyncio.run_coroutine_threadsafe(
                self.emit(
                    "nickname_updated",
                    (quid_id, str(nickname).replace('"', "")),
                ),
                self._asyncio_event_loop,
            )
        except KeyError as e:
            self.server.logger.exception(e)

    # Subscriptions
    def _set_signal_subscriptions(self, quid: Quiddity) -> None:
        """
        Arguments:
            quid {pyquid.Quiddity} -- [description]
        """

        quiddity_signals = {
            "on-nicknamed": self._on_nickname_updated,
            "on-user-data-grafted": self._on_user_tree_grafted,
            "on-user-data-pruned": self._on_user_tree_pruned,
            "on-tree-grafted": self._on_info_tree_grafted,
            "on-tree-pruned": self._on_info_tree_pruned,
            "on-connection-spec-added": self._on_connection_spec_added,
            "on-connection-spec-removed": self._on_connection_spec_removed,
            "on-connected": self._on_connected,
            "on-disconnected": self._on_disconnected,
        }

        for signal, method in quiddity_signals.items():
            if not quid.subscribe(signal, method, quid.id()):
                self.server.logger.warning(
                    f'Could not subscribe to "{signal}" '
                    f"signal of `{repr(quid)}`"
                )
            else:
                self.server.logger.info(
                    f'Subscribed to "{signal}" ' f"signal of `{repr(quid)}`"
                )

    def _set_property_subscriptions(self, quid: Quiddity) -> None:
        """Sets switcherio's property update callback for every property in the
        quiddity. If this is not called for a quiddity, the front end will not receive
        property update notifications"""
        for prop in json.loads(quid.get_info_tree_as_json(".property")):
            prop_data = {
                "quid_id": quid.id(),
                "property_name": prop["id"],
                "property_id": prop["prop_id"],
            }

            if not quid.subscribe(
                prop_data["property_name"],
                self._on_property_updated,
                prop_data,
            ):
                prop_name = prop_data["property_name"]
                self.server.logger.warning(
                    f"Could not subscribe to property `{prop_name}` "
                    f"on `{repr(quid)}`"
                )

    def _remove_property_subscriptions(self, quid: Quiddity) -> None:
        """Removes switcherio's property update callback from every property of
        the quiddity."""
        props = json.loads(quid.get_info_tree_as_json(".property"))
        for prop in props:
            if not quid.unsubscribe(prop["id"]):
                prop_id = prop["id"]
                self.server.logger.warning(
                    f'Could not unsubscribe from property "{prop_id}" '
                    f"on `{repr(quid)}`"
                )

    def _reset_property_subscriptions(self, quid_id):
        """calls remove_property_subscriptions followed by set_property_subscriptions.
        This is a bit of a sledgehammer hit to the knees
        of the dynamic property subscription :
        it removes every possible subscription from the
        quiddity and resubscribe immediately after.

        We could make this more graceful by keeping track
        of every property we have subscribe to
        for a certain quiddity and only subscribing to this
        but this is simple and effective.

        This takes a quiddity id as a parameter to make very
        sure that the pyquiddity we reset
        the subcription for is the same instance that we already
        have in the cache. Using other instances can
        be dangerous/buggy/non functionnal.
        """
        quid = self._get_quiddity_from_id(quid_id)
        self._remove_property_subscriptions(quid)
        self._set_property_subscriptions(quid)

    def _get_quiddity_from_id(self, quid_id):
        """the get_quid method of a switcher instance has issues.
        It always return a new Pyquiddity which means
        that we can never unsubscribe a callback from a quiddity
        if we have lost the original instance of the Pyquiddity.

        You're probably thinking "wait a minute, if we lost the
        original instance, shouldn't it be garbage collected by python ?"
        If PyQuid was a sane python wrapper that would be the case but
        right now Pyquiddity instances are never garbage collected,
        there is a missing reference decrementation somewhere. That
        missing decrementation is the only thing that prevents our callbacks
        from immediately being garbage collected by python.

        This function is designed to side-step both of these issues.

        If a quid id is not in the cache, it will call the original get_quid
        method and will get a new Pyquiddity instance, store it in
        the cache and return it.
        If the id is in the cache, it will return the Pyquiddity
        it already has in the cache.

        This way, we won't be creating a new Pyquiddity everytime we sneeze
        and we will be able to unsubscribe from properties and signals
        at will.

        So please, please please, never call get_quid or quiddities on
        a switcher instance, use this instead.
        """
        if quid_id in self._quiddity_cache:
            return self._quiddity_cache[quid_id]
        else:
            pyquiddity = self._sw.get_quid(quid_id)
            self._quiddity_cache[quid_id] = pyquiddity
            return pyquiddity

    # Generic Event Handlers
    # ======================

    def on_connect(self, sid: str, environ: Dict[str, Any]) -> None:
        """Special event called automatically when a client connects to the server

        The `connect` event is an ideal place to perform user authentication,
        and any necessary mapping between user entities in the application and
        the `sid` that was assigned to the client.

        The `environ` argument is a dictionary in standard WSGI format containing
        the request information, including HTTP headers.

        The `auth` argument contains any authentication details passed
        by the client, or None if the client did not pass anything.

        After inspecting the request, the connect event handler can return False
        to reject the connection with the client.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            environ {Dict[str, Any]} -- A dictionary in standard WSGI format
                                        containing the request information,
                                        including HTTP headers
        """
        version = (
            environ["QUERY_STRING"].split("&")[0].split("=")[-1] or "Undefined"
        )
        # self.enter_room(sid, self._room, self._namespace)

        asyncio.run_coroutine_threadsafe(
            self.enter_room(sid, self._room, self._namespace),
            self._asyncio_event_loop,
        )

        self._log("debug", f"connect(version: {version})", sid)

    def on_disconnect(self, sid: str) -> None:
        """Special event called automatically when a client disconnects

        Arguments:
            sid {str} -- The session identifier assigned to the client
        """
        # self.leave_room(sid, self._room, self._namespace)

        # asyncio.run_coroutine_threadsafe(
        #     self.leave_room(sid, self._room, self._namespace),
        #     self._asyncio_event_loop,
        # )

        self._log("debug", "disconnect()", sid)

    def on_connect_error(self, data):
        """Log an error when the event `connect_error` is detected

        Some API details can be checked here:
        https://python-socketio.readthedocs.io/en/latest/client.html?highlight=connect_error#connect-connect-error-and-disconnect-event-handlers for implementation details. # noqa:E501

        Arguments:
            data {str} -- Hints on the connection failure
        """
        self._log("error", f"connect_error(data: {data})")

    # Switcher API
    # ============

    def on_switcher_name(self, sid: str) -> Tuple[None, str]:
        """Get the name of the Switcher instance

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "switcher_name()", sid)
        return None, self._sw.name()

    def on_switcher_version(self, sid: str) -> Tuple[None, str]:
        """Get the version of the Switcher instance

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "switcher_version()", sid)
        return None, self._sw.version()

    def on_switcher_kinds(self, sid: str) -> Tuple[None, str]:
        """Get documentation for all kinds of quiddities

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "switcher_kinds()", sid)
        return None, self._sw.kinds_doc()

    def on_switcher_get_connections(self, sid):
        self._log("debug", "switcher_get_connections()", sid)
        return None, self._sw.get_connections().json()

    def on_switcher_quiddities(
        self, sid: str
    ) -> Tuple[Optional[str], List[dict]]:
        """Retrieves a list of existing quiddities.

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "switcher_quiddites()", sid)
        try:
            quids_list = [
                json.loads(str(quid)) for quid in self._sw.quiddities
            ]
            return None, quids_list
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), []

    def on_switcher_quiddity_id(
        self, sid: str, nickname: str
    ) -> Tuple[Optional[str], List[dict]]:
        """Get a quiddity id from its nickname.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            nickname {str} -- The quiddity nickname we want the id

        Returns:
            tuple -- The error and response for this event
                        (the quiddity id or None if not found)
        """
        self._log("debug", f"switcher_quiddity_id(nickname: {nickname})", sid)
        try:
            quid_id = self._sw.get_quid_id(nickname)
            return None, quid_id
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), []

    async def on_switcher_bundles(
        self, sid: str, bundles: Union[str, dict]
    ) -> Tuple[Optional[str], bool]:
        """Set Switcher bundles from a json encoded string description

        The description of bundles must be **valid** as specified
        in [Writing Bundles](https://gitlab.com/sat-mtl/tools/switcher/-/blob/master/doc/writing-bundles.md).

        Arguments:
            sid {str} -- The session identifier assigned to the client
            bundles {str|dict} -- The json encoded description of the bundles

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "switcher_bundles()", sid)
        try:
            if self._sw.load_bundles(
                json.loads(bundles) if isinstance(bundles, str) else bundles
            ):
                return None, True
            else:
                return "Could not load bundle configuration", False

        except Exception as e:
            self.server.logger.exception(e)
            return "Failed to load bundle configuration", False

    def on_switcher_log(
        self, sid: str, log_level: str, msg: str
    ) -> Tuple[Optional[str], bool]:
        return self._log(log_level, f"switcher_log(msg: {msg})", sid)

    def on_switcher_extra_config_paths(
        self, sid: str
    ) -> Tuple[Optional[str], List[str]]:
        """Get the paths of the extra configuration

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "switcher_extra_config.paths()", sid)
        try:
            paths = self._sw.list_extra_configs()
            return None, paths
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_switcher_extra_config_read(
        self, sid: str, name: str
    ) -> Tuple[Optional[str], str]:
        """Read the extra configuration

        Arguments:
            sid {str} -- The session identifier assigned to the client
            name {str} -- The name of the configuration file to read

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", f"switcher_extra_config.read(name: {name})", sid)
        try:
            config = self._sw.read_extra_config(name)
            return None, config
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    # Quiddity API
    # ============

    def on_quiddities_protect(
        self, sid: str, quiddity_ids: Optional[List[int]] = None
    ):
        """
        Protects the quiddities with the given ids from being removed in a call
        to reset_session (Switcher::reset_state). If called with no id, this will protect all
        existing quiddities.
        Arguments:
            sid {str} -- The session identifier assigned to the client
            quiddity_ids {Optional[List[int]]} -- Ids of the quiddities to protect.
        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug", f"quiddities.protect(quiddity_ids: {quiddity_ids})", sid
        )
        try:
            if quiddity_ids:
                self._sw.protect_quiddities(quiddity_ids)
            else:
                self._sw.protect_quiddities()
            return None, True
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_quiddity_create(
        self,
        sid: str,
        kind: str,
        nickname: Optional[str] = None,
        properties: Optional[Dict[str, Any]] = None,
        user_tree: Optional[Dict[str, Any]] = None,
    ) -> Tuple[Optional[str], Optional[str]]:
        """Create a quiddity instance

        Arguments:
            sid {str} -- The session identifier assigned to the client
            kind {str} -- The kind of quiddity to create

        Keyword Arguments:
            nickname {Optional[str]} -- [description] (default: {None})
            properties {Optional[Dict[str, Any]]} -- [description] (default: {None})
            user_tree {Optional[Dict[str, Any]]} -- [description] (default: {None})

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"quiddity_create(kind: {kind}, nickname: {nickname}, properties: {properties}, user_tree: {user_tree})",
            sid,
        )
        try:
            # return existing quiddity
            quid_id = self._sw.get_quid_id(nickname)
            if quid_id:
                self.server.logger.warning(
                    f"Trying to create a quiddity for existing nickname {nickname}, returning existing quiddity."
                )
                return None, self._get_quiddity_model(
                    self._get_quiddity_from_id(quid_id)
                )

            quid = self._sw.create(kind, nickname or "")

            # props
            if properties is not None:
                for prop, value in properties.items():
                    quid.set(prop, value)

            # user_data
            if user_tree is not None:
                for key, sub_tree in user_tree.items():
                    tree = InfoTree(json.dumps(sub_tree))
                    quid.get_user_tree().graft(key, tree)

                quid.notify_user_tree_grafted(".")

            return None, self._get_quiddity_model(quid)
        except Exception as e:
            self.server.logger.exception(e)
            return f"{e} for `{kind}` kind", None

    def on_quiddity_delete(
        self, sid: str, quid_id: str
    ) -> Tuple[Optional[str], Optional[bool]]:
        """Remove a quiddity instance

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", f"quiddity_delete(quid_id: {quid_id})", sid)
        if self._sw.remove(quid_id):
            return None, True
        return f"Could not remove quiddity identified by `{quid_id}`", False

    def on_quiddity_connect(
        self, sid: str, src_id: int, dst_id: int
    ) -> Tuple[Optional[str], bool]:
        """Connect source and destination quiddities together

        Arguments:
            sid {str} -- The session identifier assigned to the client
            src_id {int} -- The source quiddity identifier to connect to the destination
            dst_id {int} -- The destination quiddity identifier to connect the source to

        Returns:
            tuple -- The error or claw id for this event
        """
        self._log(
            "debug",
            f"quiddity_connect(src_id: {src_id}, dst_id: {dst_id})",
            sid,
        )
        src = self._get_quiddity_from_id(src_id)
        dst = self._get_quiddity_from_id(dst_id)
        try:
            fclaw = dst.try_connect(src)
            return None, fclaw.id()
        except Exception as e:
            self.server.logger.exception(e)
            return (
                f"Could not connect quiddities `{src.nickname()}` and `{dst.nickname()}`",
                False,
            )

    def _get_follower_claw(self, dst, sfid):
        """
        Arguments:
            dst {pyquiddity} -- the follower pyquiddity
            sfid {int} -- the sfid of the pyfclaw to get
        Return:
            pyfclaw -- the claw object for the sfid
        """
        fclaws = [
            claw for claw in dst.get_follower_claws() if claw.id() == sfid
        ]
        if len(fclaws) != 1:
            raise Exception("Could not find claw")
        return fclaws[0]

    def on_quiddity_connect_sfid(
        self, sid: str, src_id: str, dst_id: str, sfid: int
    ) -> Tuple[Optional[str], bool]:
        """Connect the source to a specific sfid of the destination

        Arguments:
            sid {str} -- The session identifier assigned to the client
            src_id {str} -- The source quiddity identifier to connect to the destination
            dst_id {str} -- The destination quiddity identifier to connect the source to
            sfid {str} -- the sfid to connect to

        Returns:
            tuple -- The error or claw id for this event
        """
        self._log(
            "debug",
            f"quiddity_connect_sfid(src_id: {src_id}, dst_id: {dst_id})",
            sid,
        )
        src = self._get_quiddity_from_id(src_id)
        dst = self._get_quiddity_from_id(dst_id)
        try:
            fclaw = self._get_follower_claw(dst, sfid)
            # since we can possibly connect to a meta sfid, switcher can decide that
            # the id to which we ask to be connected to is not the one that we will really
            # be connected to.
            connected_claw = fclaw.connect_quid(src)
            return None, connected_claw.id()
        except Exception as e:
            self.server.logger.exception(e)
            return (
                f"Could not connect quiddities `{src.nickname()}` and `{dst.nickname()}` to sfid `{sfid}`",
                False,
            )

    def on_quiddity_disconnect(
        self, sid: str, dst_id: int, fclaw_id: Optional[int]
    ) -> Tuple[Optional[str], bool]:
        """Disconnect a destination quiddity and specific connection

        Arguments:
            sid {str} -- The session identifier assigned to the client
            dst_id {int} -- The destination quiddity identifier that is connected
            fclaw_id {int} -- The connected follower claw id

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"quiddity_disconnect(dst_id: {dst_id}, fclaw_id: {fclaw_id})",
            sid,
        )
        dst = self._get_quiddity_from_id(dst_id)
        is_disconnected = False

        try:
            fclaw = self._get_follower_claw(dst, fclaw_id)
            is_disconnected = fclaw.disconnect()
            return None, is_disconnected
        except Exception as e:
            self.server.logger.exception(e)
            return (
                f"Could not disconnect quiddity `{dst.nickname()}`",
                is_disconnected,
            )

    async def on_quiddity_invoke(
        self,
        sid: str,
        quid_id: str,
        method_name: str,
        method_args: List[str] = [],
    ) -> Tuple[Optional[str], Optional[Any]]:
        """Invoke a method of a quiddity

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            method_name {str} -- The name of the method to invoke
            method_args {List[str] = []} -- The list of arguments for the method

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"quiddity_invoke(quid_id: {quid_id}, method_name: {method_name}, method_args: {method_args})",
            sid,
        )
        try:
            quid = self._get_quiddity_from_id(quid_id)
            return None, quid.invoke(method_name, method_args)
        except TypeError as e:
            return f"Could not invoke method `{e}`", None
        except KeyError:
            return f"Quiddity `{quid_id}` does not exist", None

    def on_quiddity_shmpath(
        self, sid: str, quid_id: str, claw_label: str
    ) -> Tuple[Optional[str], Optional[str]]:
        """Retrieves shmpath of a quiddity writer claw by label.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            claw_label {str} -- The writer claw label
        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"quiddity_shmpath(quid_id: {quid_id}), claw_label: {claw_label}",
            sid,
        )
        try:
            writer_claws = self._get_quiddity_from_id(
                quid_id
            ).get_writer_claws()
            for writer_claw in writer_claws:
                if writer_claw.label() == claw_label:
                    return None, writer_claw.shmpath()
            return (
                f"Could not find label `{claw_label}` in quid_id `{quid_id}.",
                None,
            )
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), None

    # Tree API
    # ========

    def on_info_tree_get(
        self, sid: str, quid_id: str, tree_path: Optional[str] = None
    ) -> Tuple[Optional[str], Optional[str]]:
        """Retrieves the info tree (read only) of a quiddity.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            tree_path {Optional[str]} -- Get a value from a path of the tree

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"info_tree_get(quid_id: {quid_id}, tree_path: {tree_path})",
            sid,
        )
        try:
            quid = self._get_quiddity_from_id(quid_id)
            info_tree = {}

            if not tree_path:
                full_tree = quid.get_info_tree_as_json()
                info_tree = json.loads(str(full_tree))
            else:
                info_tree = quid.get_info(tree_path)

            return None, info_tree
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), None

    def on_user_tree_get(
        self, sid: str, quid_id: str, tree_path: Optional[str] = None
    ) -> Tuple[Optional[str], Optional[str]]:
        """Retrieves the user tree of a quiddity.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            tree_path {Optional[str]} -- Get a value from a path of the tree
        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"user_tree_get(quid_id: {quid_id}, tree_path: {tree_path})",
            sid,
        )
        try:
            quid = self._get_quiddity_from_id(quid_id)
            user_tree = quid.get_user_tree()
            if user_tree == "null":
                user_tree = {}
            elif tree_path:
                user_tree = quid.get_user_tree_as_json(tree_path)
            return None, json.loads(str(user_tree))
        except ValueError:
            # if this is not a json, this is None or a primitive value
            return None, user_tree
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), None

    def on_user_tree_graft(
        self, sid: str, quid_id: str, path: str, value: str
    ) -> Tuple[Optional[str], Optional[str]]:
        """Graft the user tree of a quiddity.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            path {str} -- The path to graft
            value {str} -- The new value of the grafted path

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"user_tree_graft(quid_id: {quid_id}, path: {path}, value: {value})",
            sid,
        )
        try:
            self._get_quiddity_from_id(quid_id).get_user_tree().graft(
                path, value
            )
            self._get_quiddity_from_id(quid_id).notify_user_tree_grafted(path)
            user_tree = self._get_quiddity_from_id(quid_id).get_user_tree()
            return None, json.loads(str(user_tree))
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), None

    def on_user_tree_prune(
        self, sid: str, quid_id: str, path: str
    ) -> Tuple[Optional[str], Optional[bool]]:
        """Prune the user tree of a quiddity.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            path {str} -- The path to graft

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug", f"user_tree_prune(quid_id: {quid_id}, path: {path})", sid
        )
        try:
            result = (
                self._get_quiddity_from_id(quid_id).get_user_tree().prune(path)
            )
            self._get_quiddity_from_id(quid_id).notify_user_tree_pruned(path)
            return None, result
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), None

    def on_connection_specs_get(
        self, sid: str, quid_id: str
    ) -> Tuple[Optional[str], Optional[str]]:
        """Retrieves the connection specs (read only) of a quiddity.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", f"connection_specs_get(quid_id: {quid_id})", sid)
        try:
            connection_specs = self._get_quiddity_from_id(
                quid_id
            ).get_connection_specs()
            return None, json.loads(str(connection_specs))
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), None

    # Property API
    # ============

    async def on_property_get(
        self, sid: str, quid_id: str, property_name: str
    ) -> Tuple[Optional[str], Optional[Any]]:
        """Get the value of a property for a given quiddity

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            property_name {str} -- The name of the property to get

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"property_get(quid_id: {quid_id}, property_name: {property_name})",
            sid,
        )
        try:
            quid = self._get_quiddity_from_id(quid_id)
            return None, quid.get(property_name)
        except ValueError as e:
            return f"Could not get property value: `{e}`", None
        except KeyError:
            return f"Quiddity `{quid_id}` does not exist", None

    async def on_property_set(
        self, sid: str, quid_id: str, property_name: str, value: str
    ) -> Tuple[Optional[str], Optional[bool]]:
        """Set the value of a property for a given quiddity

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            property_name {str} -- The name of the property to get
            value {str} -- The value to assign to the property

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"property_set(quid_id: {quid_id}, property_name: {property_name}, value: {value})",
            sid,
        )
        try:
            quid = self._get_quiddity_from_id(quid_id)
            if quid.set(property_name, value):
                return None, quid.get(property_name)
            return (
                f"Could not set value `{value}`"
                f"for property `{property_name}`",
                None,
            )
        except KeyError:
            return f"Quiddity `{quid_id}` does not exist", None

    # Nickname API
    # ============

    async def on_nickname_get(
        self, sid: str, quid_id: str
    ) -> Tuple[Optional[str], Optional[str]]:
        """Get a given quiddity nickname

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier

        Returns:
            tuple -- The error and response for this event
        """
        # mute nickname.get event from logging
        # self._log("debug", f"nickname_get(quid_id: {quid_id})", sid)
        try:
            quid = self._get_quiddity_from_id(quid_id)
            return None, quid.nickname()
        except KeyError:
            return f"Quiddity `{quid_id}` does not exist", None

    async def on_nickname_set(
        self, sid: str, quid_id: str, nickname: str
    ) -> Tuple[Optional[str], Optional[bool]]:
        """Set a given quiddity nickname

        Arguments:
            sid {str} -- The session identifier assigned to the client
            quid_id {str} -- The quiddity identifier
            nickname {str} -- The new nickname for the quiddity

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"nickname_set(quid_id: {quid_id}, nickname: {nickname})",
            sid,
        )
        try:
            quid = self._get_quiddity_from_id(quid_id)
            if quid.set_nickname(nickname):
                return None, True
            return (
                f"Could not set nickname `{nickname}` "
                f"for quiddity `{quid_id}`"
            ), False
        except KeyError:
            return f"Quiddity `{quid_id}` does not exist", False

    # Session API
    # ===========

    def on_session_reset(self, sid: str):
        """Resets initial session state and deletes all unprotected quiddities.

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "session_reset()", sid)
        try:
            self._sw.reset_state()
            return None, True
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_session_clear(self, sid: str):
        """Remove all Quiddities of the current session.

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "session_clear()", sid)
        try:
            for qid in self._sw.list_ids():
                self._sw.remove(qid)
            return None, True
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_session_list(self, sid: str):
        """Get a list of all session files.

        Arguments:
            sid {str} -- The session identifier assigned to the client

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", "session_list()", sid)
        try:
            return None, self._sw.session.list()
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_session_save_as(self, sid: str, session_name: str):
        """Save a session as file.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            session_name {str} -- The name of the session

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug", f"session_save_as(session_name: {session_name})", sid
        )
        try:
            return None, self._sw.session.save_as(session_name)
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_session_remove(self, sid: str, session_name: str):
        """Remove the file of a session.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            session_name {str} -- The name of the session

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug", f"session_remove(session_name: {session_name})", sid
        )
        try:
            return None, self._sw.session.remove(session_name)
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_session_load(self, sid: str, session_name: str):
        """Load a session.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            session_name {str} -- The name of the session

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", f"session_load(session_name: {session_name})", sid)
        try:
            return None, self._sw.session.load(session_name)
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_session_read(self, sid: str, session_name: str):
        """Read the content of a session.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            session_name {str} -- The name of the session

        Returns:
            tuple -- The error and response for this event
        """
        self._log("debug", f"session_read(session_name: {session_name})", sid)
        try:
            return None, self._sw.session.read(session_name)
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_session_write(
        self, sid: str, session_content: str, session_name: str
    ):
        """Write content to a session file.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            session_content {str} -- Content to write to session file
            session_name {str} -- The name of the session

        Returns:
            tuple -- The error and response for this event
        """
        self._log(
            "debug",
            f"session_write(session_name: {session_name}, session_content: {session_content})",
            sid,
        )
        try:
            return None, self._sw.session.write(session_content, session_name)
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False

    def on_image_read(self, sid: str, image_filepath: str):
        """Read the content of a image file.

        Arguments:
            sid {str} -- The session identifier assigned to the client
            image_filepath {str} -- Full path of image file to read

        Returns:
            tuple -- The error and response for this event
        """
        # mute image_read event from logging
        # self._log("debug", f"image_read(image_filepath: {image_filepath})", sid)
        try:
            image_bytes = None
            with open(image_filepath, "rb") as image_file:
                image_bytes = image_file.read()
            return None, image_bytes
        except Exception as e:
            self.server.logger.exception(e)
            return str(e), False
